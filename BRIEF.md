MAIN POINTS
===========
* AJAX Login
* Multiple Image Upload
* Photo Page
* Paging

UNDER CONSTRUCTION
==================
* working on the admin page.
	* flagged photos.
	* members to keep an eye on.
	* admin privileges on other pages?
* need to implement paging for more lists of photos.
* need to implement paging for odd comments etc..
* friends.
* browse by category.

WHAT NEXT?
==========
* Galleries
* Extensive Search
* Geographical Photo Tagging
* AJAX Implementation of Photo Actions
