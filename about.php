<?
/* 
* PHOTOMONKEY ABOUT PAGE
* ABOUT.PHP
*
* Author: Daniel Edwards w1056952
*/

include 'dbconnect.php';

//these lines of code assign dollar variables to the session values for use in the code.
$sessUsername = $_SESSION['username'];
$sessUserId = $_SESSION['userId'];
$sessUserType = $_SESSION['userType'];

//creates a single variable to deduce whether any user is a logged in member or not.
if($sessUsername && $sessUserId && $sessUserType) $loggedIn = true;
else $loggedIn = false;

$area = $_GET['area'];
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<!--head contains the .css file link, the favicon.ico link and the title-->
	<link rel="shortcut icon" href="images/favicon<? echo colourOfTheDay(); ?>.ico"/>
	<link rel="stylesheet" href="siteStyle.css"/>
	<script language="javascript" src="javascript.js"></script>
	<title>Admin</title>
</head>

<body>
<div id="wrapper">
	<!--HEADER-->
	<div id="logo" onClick="location.href='index.php'">
		<img src="images/logo<? echo colourOfTheDay(); ?>.png" alt="logo image" title="Logo" width="50px" /> <h1>photomonkey</h1>
	</div>

	<div id="search">
		<form action="search.php?area=tags" method="post" >
				<input type="text" name="searchBox" class="input" value="Search..." onClick="this.value=''"/>
				<input type="submit" value="Go" class="button" />
		</form>
	</div>
	
	<div id="topNav">
		<ul id="menu">
			<li><a href="index.php" id="tabLink">home</a></li>
			<li>
				<a href="explore.php?content=potd" id="tabLink" onmouseover="menuOpen('dropDownMenu')" onmouseout="menuClose()">let's go exploring...</a>
				<div id="dropDownMenu" onmouseover="cancelClose('dropDownMenu')" onmouseout="menuClose()">
					<a href="explore.php?content=bbint">by interestingness</a>
					<a href="explore.php?content=bbrec">by most resent</a>
			        <a href="explore.php?content=bbcat">by category</a>
			        <a href="explore.php?content=bbspec">by spectrum</a>
			        <a href="explore.php?content=bbcotd">by colour of the day</a>
					<a href="explore.php?content=rand">randomly</a>
	        	</div>
			</li>
			<li>
				<a href="profile.php?username=<? echo $sessUsername; ?>" id="tabLink" onMouseOver="menuOpen('dropDownMenu2')" onMouseOut="menuClose()" >your profile</a>
				<div id="dropDownMenu2" onMouseOver="cancelClose('dropDownMenu2')" onMouseOut="menuClose()">
					<a href="userPhotos.php?username=<? echo $sessUsername;?>">your photos</a>
			        <a href="editAccount.php?userId=<? echo $sessUserId; ?>">adjust account</a>
	        	</div>
			</li>
			<li><a href="upload.php" id="tabLink">upload</a></li>
		</ul>

		<ul id="menu" class="floatRight">
			<?
			if($loggedIn){
				echo '<li><a href="logout.php" id="tabLink">logout '.$sessUsername.'</a></li>';
				if($sessUserType == 'a') {
					echo '<li><a href="admin.php" id="tabLink">admin</a></li>';
				}
			}
			else{
				echo '<li><a href="login.php" id="tabLink">login / sign up</a></li>';
			}
			?>
		</ul>
	</div>
	
	<!--MAIN-->
	<div id="main">
		<h2>About Us</h2><br />
		Well... about me.<br />
		<div id="full">
			<?
			if($area == "terms") {
				echo "<h2>Terms and Condititons</h2>";
				echo "<div id=\"floatRight\"><br />";
				echo "<a href=\"about.php?area=photos\">about</a> | T's & C's";
				echo "</div>";
				echo "<br /><div class=\"hr\"></div>";

				echo "<h3>Website Use</h3><br />";
				echo "<br />";
				echo "<h3>Copyright</h3><br />";
				echo "Any users must not upload any content that they know is the property of a third party. Photomonkey operates a flagging system that enforces copyright infringement. If photomonkey has determined any user to be a persistent copyright violator, photomonkey has the right to terminate their account without any notice. Any issues regarding this matter can be taken up with photomonkey directly. We take this issue very seriously.<br /><br />";
				echo "<h3>Community Etiquette</h3><br />";
				echo "<br />";
				echo "<h3>Uploaded Content</h3><br />";
				echo "<br />";

				
			}
			else {
				echo "<h2>Photomonkey</h2>";
				echo "<div id=\"floatRight\"><br />";
				echo "about | <a href=\"about.php?area=terms\">T's & C's</a>";
				echo "</div>";
				echo "<br /><div class=\"hr\"></div>";

				echo "Photomonkey is the final year project of Daniel Edwards, a student studying Computer Science at the University of Westminster.<br />Photomonkey's view is to create an engaging and functional website for members of the photographic community to become involved in and use as a platform for sharing their own work and also drawing inspiration from others. The website is to focus on the photographer and their photos, to exhibit photographers work and personality as best as possible shown through the sites design and functionality at every level; 'The photo is the star'. All this is to be achieved in a user-friendly environment, through a website with a... playful temperament.";
				
			}
			?>
		<div id="clear"></div>
		</div>
	</div>
	
	<!--FOOTER-->
	<ul id="menu">
		<li><a href="about.php" id="currentTabLink">about</a></li>
	</ul>
</div>
</body>

</html>