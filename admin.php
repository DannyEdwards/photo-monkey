<?
/* 
* PHOTOMONKEY ADMIN PAGE
* ADMIN.PHP
*
* Author: Daniel Edwards w1056952
*/
include 'dbconnect.php';
protectAdminPage();

//these lines of code assign dollar variables to the session values for use in the code.
$sessUsername = $_SESSION['username'];
$sessUserId = $_SESSION['userId'];
$sessUserType = $_SESSION['userType'];

//creates a single variable to deduce whether any user is a logged in member or not.
if($sessUsername && $sessUserId && $sessUserType) $loggedIn = true;
else $loggedIn = false;

$newsArea = $_GET['area'];

if($newsArea == "members"){
	$membersQuery = "SELECT userId, username, avatar, offences
					FROM photomonkey.user
					WHERE offences > 0
					ORDER BY offences DESC";
	$membersResult = mysql_query($membersQuery);
}
else {
	$photosQuery = "SELECT p.photoId, p.ownerId, u.username, p.title, p.fileName, DATE_FORMAT(p.dateAdded, '%D %M %Y at %l:%i%p')
					FROM photo p
					JOIN user u
					ON p.ownerId = u.userId
					WHERE flagged = 'y'
					ORDER BY dateAdded DESC";
	$photosResult = mysql_query($photosQuery);
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<!--head contains the .css file link, the favicon.ico link and the title-->
	<link rel="shortcut icon" href="images/favicon<? echo colourOfTheDay(); ?>.ico"/>
	<link rel="stylesheet" href="siteStyle.css"/>
	<script language="javascript" src="javascript.js"></script>
	<title>Admin</title>
</head>

<body>
<div id="wrapper">
	<!--HEADER-->
	<div id="logo" onClick="location.href='index.php'">
		<img src="images/logo<? echo colourOfTheDay(); ?>.png" alt="logo image" title="Logo" width="50px" /> <h1>photomonkey</h1>
	</div>

	<div id="search">
		<form action="search.php?area=tags" method="post" >
				<input type="text" name="searchBox" class="input" value="Search..." onClick="this.value=''"/>
				<input type="submit" value="Go" class="button" />
		</form>
	</div>
	
	<div id="topNav">
		<ul id="menu">
			<li><a href="index.php" id="tabLink">home</a></li>
			<li>
				<a href="explore.php?content=potd" id="tabLink" onmouseover="menuOpen('dropDownMenu')" onmouseout="menuClose()">let's go exploring...</a>
				<div id="dropDownMenu" onmouseover="cancelClose('dropDownMenu')" onmouseout="menuClose()">
					<a href="explore.php?content=bbint">by interestingness</a>
					<a href="explore.php?content=bbrec">by most resent</a>
			        <a href="explore.php?content=bbcat">by category</a>
			        <a href="explore.php?content=bbspec">by spectrum</a>
			        <a href="explore.php?content=bbcotd">by colour of the day</a>
					<a href="explore.php?content=rand">randomly</a>
	        	</div>
			</li>
			<li>
				<a href="profile.php?username=<? echo $sessUsername; ?>" id="tabLink" onMouseOver="menuOpen('dropDownMenu2')" onMouseOut="menuClose()" >your profile</a>
				<div id="dropDownMenu2" onMouseOver="cancelClose('dropDownMenu2')" onMouseOut="menuClose()">
					<a href="userPhotos.php?username=<? echo $sessUsername;?>">your photos</a>
			        <a href="editAccount.php?userId=<? echo $sessUserId; ?>">adjust account</a>
	        	</div>
			</li>
			<li><a href="upload.php" id="tabLink">upload</a></li>
		</ul>

		<ul id="menu" class="floatRight">
			<?
			if($loggedIn){
				echo '<li><a href="logout.php" id="tabLink">logout '.$sessUsername.'</a></li>';
				if($sessUserType == 'a') {
					echo '<li><a href="admin.php" id="currentTabLink">admin</a></li>';
				}
			}
			else{
				echo '<li><a href="login.php" id="tabLink">login / sign up</a></li>';
			}
			?>
		</ul>
	</div>
	
	<!--MAIN-->
	<div id="main">
		<h2>Administration</h2><br />
		Time to get serious...<br />
		<div id="full">
			<?
			if($newsArea == "members") {
				echo "<h2>Who have we got here then?</h2>";
				echo "<div id=\"floatRight\"><br />";
				echo "<a href=\"admin.php?area=photos\">flagged photos</a> | naughty members";
				echo "</div>";
				echo "<br /><div class=\"hr\"></div>";

				$query = "";

				if(!mysql_fetch_assoc($tagsResult)) echo "<i>Your search returned with no results.</i><br />";
				else {
					$tagsResult = mysql_query($tagsQuery);
					
					while ($record = mysql_fetch_assoc($tagsResult)){
						$photoId = $record['photoId'];
						$ownerId = $record['ownerId'];
						$fileName = $record['fileName'];
						$tags = $record['tags'];
						$title = $record['title'];
		
						echo "<div id=\"floatLeft\">";
						echo "<a href=\"photo.php?photoId=$photoId\"><img src=\"photos/$ownerId/thumb_$fileName\" alt=\"a photo matching your query\" width=100 border=0 /></a>";
						echo "</div>";
						echo "&nbsp Title: <a href=photo.php?photoId=$photoId />$title</a><br />";
						echo "&nbsp Tags: $tags<br /><br /><br /><br /><br /><br />";
					}
				}
			}
			else {
				echo "<h2>What is this tomfoolery?</h2>";
				echo "<div id=\"floatRight\"><br />";
				echo "flagged photos | <a href=\"admin.php?area=members\">naughty members</a>";
				echo "</div>";
				echo "<br /><div class=\"hr\"></div>";

				if(!mysql_fetch_assoc($photosResult)) echo "<i>Your search returned with no results.</i><br />";
				else {
					$photosResult = mysql_query($photosQuery);
					
					while ($record = mysql_fetch_assoc($photosResult)){
						$photoId = $record['photoId'];
						$ownerId = $record['ownerId'];
						$username = $record['username'];
						$fileName = $record['fileName'];
						$title = $record['title'];
						$dateAdded = $record["DATE_FORMAT(p.dateAdded, '%D %M %Y at %l:%i%p')"];
		
						echo "<div id=\"floatLeft\">";
						echo "<a href=\"photo.php?photoId=$photoId\"><img src=\"photos/$ownerId/$fileName\" alt=\"a potential photo violation\" width=250 border=0 /></a>";
						echo "</div>";
						echo "&nbsp Owner: <a href=profile.php?username=$username />$username</a><br />";
						echo "&nbsp Uploaded: $dateAdded<br /><br /><br /><br /><br /><br />";
					}
				}
			}
			?>
		<div id="clear"></div>
		</div>
	</div>
	
	<!--FOOTER-->
	<ul id="menu">
		<li><a href="about.php" id="tabLink">about</a></li>
	</ul>
</div>
</body>

</html>