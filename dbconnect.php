<?
/* 
* PHOTOMONKEY DATABASE CONNECTIONS
* DBCONNECT.PHP
*
* Author: Daniel Edwards w1056952
*/

//this file not only deals with the database connection but also holds functions that are used throughout the website.
//THIS IS FOR THE LOCALHOST
$host = "localhost";
$username = "photomonkey_user";
$password = "THaCp6Fv7AFPSLHJ";

$connection = mysql_connect($host,$username,$password) or die('Could not connect: '.mysql_error());
$database = mysql_select_db("photomonkey", $connection) or die('Could not connect to database "photomonkey".');

session_start();

function protectPage() {
	//checks for cookies. If present, the session for the user is initiated for this page.
	if(isset($_COOKIE['userId']) && isset($_COOKIE['username']) && isset($_COOKIE['userType'])){
		$_SESSION['userId'] = $_COOKIE['userId'];
		$_SESSION['username'] = $_COOKIE['username'];
		$_SESSION['userType'] = $_COOKIE['userType'];
	}


	//if the session is not initiated the user is not authorised to this page so they are redirected to login.php.
	if (!isset($_SESSION['userId']) && !isset($_SESSION['username']) && !isset($_SESSION['userType'])){
		header("Location: login.php?redirection=restrictedContent");
	}
}

function protectAdminPage() {
	//checks for cookies. If present, the session for the user is initiated for this page.
	if(isset($_COOKIE['userId']) && isset($_COOKIE['username']) && isset($_COOKIE['userType'])){
		$_SESSION['userId'] = $_COOKIE['userId'];
		$_SESSION['username'] = $_COOKIE['username'];
		$_SESSION['userType'] = $_COOKIE['userType'];
	}


	//if the session is not initiated the user is not authorised to this page so they are redirected to login.php.
	if (!isset($_SESSION['userId']) && $_SESSION['userType'] != 'a'){
		$error = "Access restricted to admin page";
		$message = "Waaay! You really were not meant to be there.";
		header("Location: error.php?error=$error&message=$message");
	}
}

function deleteDirectory($dir) {
	//this function is from http://php.net/manual/en/function.rmdir.php.
    $files = glob( $dir . '*', GLOB_MARK );
    foreach( $files as $file ){
        if( is_dir( $file ) )
            deleteDirectory( $file );
        else
            unlink( $file );
    }
  
    if (is_dir($dir)) rmdir( $dir );
  
}

function setCookies() {
	//checks for cookies. If present, the session for the user is initiated for this page.
	if(isset($_COOKIE['userId']) && isset($_COOKIE['username'])){
		$_SESSION['userId'] = $_COOKIE['userId'];
		$_SESSION['username'] = $_COOKIE['username'];
		$_SESSION['userType'] = $_COOKIE['userType'];
	}
}

function protectPhoto($photoAccess) {
	if(isset($_SESSION['userId']) && isset($_SESSION['username'])){
		$member = true;	
	}
	else $member = false;

	if ($photoAccess == 'o') {
		//'open': scaled open to members and non members but full res only open to members.
		if($member){
			$fullResAvailable = true;
		}
		else $fullResAvailable = false;
	}
	elseif ($photoAccess == 's') {
		//'semi-open': scaled and full res are only open to members
		if(!$member){
			header("Location: login.php?redirection=restrictedContent");
		}
		else $fullResAvailable = true;
	}
	elseif ($photoAccess == 'r') {
		//'restricted': only open to members but no full res
		if(!$member){
			header("Location: login.php?redirection=restrictedContent");
		}
		else $fullResAvailable = false;
	}
	else {
		$error = "Photo Access properties for this photo are not properly assigned.";
		$message = "There is an error with this photo";
		header("Location: error?message=$message&error=$error");
	}
	
	return $fullResAvailable;
}

function colourOfTheDay() {
	//colour of the day.
	if (date("N") == 1)return "R";
	elseif(date("N") == 2)return "O";
	elseif(date("N") == 3)return "Y";
	elseif(date("N") == 4)return "G";
	elseif(date("N") == 5)return "B";
	elseif(date("N") == 6)return "I";
	elseif(date("N") == 7)return "V";
	else echo "You are in another dimension!";
}


function querySecurity($query) {
	//this function prevents query injection by checking for the delimiter. If the delimiter is present, something is going on...
	if(strstr($query, ";")){
		$error = "You fail.";
		$message = "Stop trying to hack my website!";

		header("Location: error.php?error=$error&message=$message");
	}
}
?>