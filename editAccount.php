<?
/* 
* PHOTOMONKEY ACCOUNT EDITING PAGE
* EDITACCOUNT.PHP
*
* Author: Daniel Edwards w1056952
*/

session_start();
include 'dbconnect.php';
protectPage();

//these lines of code assign dollar variables to the session values for use in the code.
$sessUsername = $_SESSION['username'];
$sessUserId = $_SESSION['userId'];
$sessUserType = $_SESSION['userType'];

//creates a single variable to deduce whether any user is a logged in member or not.
if($sessUsername && $sessUserId && $sessUserType) $loggedIn = true;
else $loggedIn = false;

$userId = $_GET['userId'];

if($sessUserId != $userId){
	$message = "Curiosity killed the cat...";
	$error = "You were trying to access a page you don't have access to.";
	header("Location: error.php?message=$message&error=$error");
}

$query = "SELECT username, password, firstName, lastName, email, otherInfo, avatar FROM photomonkey.user WHERE userId = '$userId'";

querySecurity($query);

$result = mysql_query($query);
$record = mysql_fetch_assoc($result);

$username = $record['username'];
$password = $record['password'];
$firstName = $record['firstName'];	
$lastName = $record['lastName'];
$email = $record['email'];
$otherInfo = $record['otherInfo'];
$avatar = $record['avatar'];
		
		
if (isset($_POST['updatePassword'])){
	//code for updating the password.
	$newPassword = crypt($_POST['password1'], "pw");

	$query = "UPDATE photomonkey.user SET password='$newPassword' WHERE userId='$userId'";
	
	mysql_query($query);
		
	$passwordSet = true;
}
elseif (isset($_POST['updateEmail'])){
	//code for updating the email.
	$newEmail = $_POST['email'];

	$query = "UPDATE photomonkey.user SET email='$newEmail' WHERE userId='$userId'";
	
	mysql_query($query);
		
	$emailSet = true;
}
elseif (isset($_POST['updateOther'])){
	//code for updating all the other inf o.
	$newFirstName = $_POST['firstName'];
	$newLastName = $_POST['lastName'];
	$newOtherInfo = $_POST['otherInfo'];
	
	$query = "UPDATE photomonkey.user SET firstName='$newFirstName', lastName='$newLastName', otherInfo='$newOtherInfo' WHERE userId='$userId'";
	
	mysql_query($query);
	
	if($_FILES["avatar"]["error"] == 4) {
		//if there is no avatar sent, nothing is done. The old avatar remains.
	}
	else {
		//if an avatar has been submitted in the previous form then we can begin to deal with the file wich will result in a scaled down thumbnail of the original image.
		//max file size in bytes.
		$maxFileSize = 5*1024*1024;
	
		if ($_FILES["avatar"]["type"] == "image/jpeg") $extension = ".jpg";
		if ($_FILES["avatar"]["type"] == "image/gif") $extension = ".gif";
		if ($_FILES["avatar"]["type"] == "image/pjpeg") $extension = ".png";
		
		//these if statements determine if the files being uploaded are of a valid file type and size.
		//the result is stored into a variable to be used next.
		if ($extension && ($_FILES["avatar"]["size"] < $maxFileSize)) $fileTypeValid = true; 
		else $fileTypeValid = false;
	
		//if the file type is valid we continue with the upload.
		//error handling standards from w3schools.com.
		if ($fileTypeValid){
			if ($_FILES["avatar"]["error"] > 0){
				$error = $_FILES["avatar"]["error"];
				$message = "Somethings gone wrong with your upload. Give it another go.";
				header("Location: error.php?message=$message&error=$error");
			}
			else{
				//create the new file name called "avatar" plus extension.
				$fileName = "avatar".$extension;

				//before anything else is done, the old avatar is removed.
				unlink("photos/$userId/$avatar");

				if (file_exists("photos/$userId/".$fileName)){
					//this should actually never occur.
					$error = "There was a problem with uploading your avatar, in fact, this should never happen!";
					$message = "Something Went Wrong.";
					header("Location: error.php?error=$error&message=$message");
 				}
    			else {
					//everything seems fine so now we start the image manipulation.
					//The following code for image manipulation is a portion from upload.php.

					//get image sizes and type information.
					$size = getimagesize($_FILES['avatar']['tmp_name']);
					//$size[0] is width.
					//$size[1] is height.
					//$size[2] is type.
				
					//load image into $image variable. supports .jpeg, .gif and .png.
					if($extension == ".jpg") {
						$image = imagecreatefromjpeg($_FILES['avatar']['tmp_name']);
					}
					elseif($extension == ".gif") {
						$image = imagecreatefromgif($_FILES['avatar']['tmp_name']);
					}
					elseif($extension == ".png") {
						$image = imagecreatefrompng($_FILES['avatar']['tmp_name']);
					}

					if($size[0] > $size[1]) {
						//landscape.
						$biggestSide = $size[0]; //find biggest length to be used later.
					}
					else {
						//portrait.
						$biggestSide = $size[1]; //find biggest length to be used later.
					}

					//now we create the square thumbnail image form the original image desegnated to be the avatar.
					//The crop size will be a third that of the largest side 
					$cropPercent = 0.66; // This will zoom in to 66% zoom (crop)
					$cropWidth   = $biggestSide*$cropPercent; 
					$cropHeight  = $biggestSide*$cropPercent;            
			
					//getting the top left coordinate
					$x = ($size[0]-$cropWidth)/2;
					$y = ($size[1]-$cropHeight)/2;
				
					$avatarSize = 100; // will create a 100 x 100 thumb
					$avatarImage = imagecreatetruecolor($avatarSize, $avatarSize); 
					
					imagecopyresampled($avatarImage, $image, 0, 0,$x, $y, $avatarSize, $avatarSize, $cropWidth, $cropHeight);
					
					$jpegCompression = 100;
					$permissions=null;

					//save square thumb photo.
					if($extension == ".jpg") {
						imagejpeg($avatarImage,"photos/".$userId."/".$fileName, $jpegCompression);
					}
					elseif($extension == ".gif") {
						imagegif($avatarImage,"photos/".$userId."/".$fileName);         
					}
					elseif($extension == ".png") {
						imagepng($avatarImage,"photos/".$userId."/".$fileName);
					}
   
					if($permissions != null) {
						chmod($_FILES['avatar']['tmp_name'],$permissions);
					}

					//now add in the avatar info into the database.
					$query = "UPDATE photomonkey.user SET avatar = '$fileName' WHERE userId = '$userId'";
					mysql_query($query);
				}

			}
		}
		else{
			$message = "Invalid file type.";
			$error = "Avatar image must be .jpeg, .gif or a .png.";
			header("Location: error.php?message=$message&error=$error");
		}
	}
	
	$otherSet = true;
}
elseif (isset($_POST['DELETE'])){
	
	//remove all trace of user.
	$query = "DELETE FROM photomonkey.photo WHERE ownerId='$userId'";
	mysql_query($query);
	$query = "DELETE FROM photomonkey.comment WHERE userId='$userId'";
	mysql_query($query);
	$query = "DELETE FROM photomonkey.friend WHERE userId1='$userId' OR userId2='$userId'";
	mysql_query($query);
	$query = "DELETE FROM photomonkey.favourite WHERE userId='$userId'";
	mysql_query($query);
	$query = "DELETE FROM photomonkey.user WHERE userId='$userId'";
	mysql_query($query);
	
	//delete directory.
	deleteDirectory("photos/$userId");
	
	//destroy sessions and cookies.
	session_destroy();
	if(isset($_COOKIE['userId']) && isset($_COOKIE['username']) && isset($_COOKIE['userType'])){
		setcookie("userId","",-1,"/");
		setcookie("username","",-1,"/");
		setcookie("userType","",-1,"/");
	}

	//send to home.
	header("Location: index.php");
}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<!--head contains the .css file link, the favicon.ico link and the title-->
	<link rel="shortcut icon" href="images/favicon<? echo colourOfTheDay(); ?>.ico"/>
	<link rel="stylesheet" href="siteStyle.css"/>
	<script language="javascript" src="javascript.js"></script>
	<title>Edit Account Info</title>
</head>

<body>
<div id="wrapper">
	<!--HEADER-->
	<div id="logo" onClick="location.href='index.php'">
		<img src="images/logo<? echo colourOfTheDay(); ?>.png" alt="logo image" title="Logo" width="50px" /> <h1>photomonkey</h1>
	</div>

	<div id="search">
		<form action="search.php?area=tags" method="post" >
				<input type="text" name="searchBox" class="input" value="Search..." onClick="this.value=''"/>
				<input type="submit" value="Go" class="button" />
		</form>
	</div>
	
	<div id="topNav">
		<ul id="menu">
			<li><a href="index.php" id="tabLink">home</a></li>
			<li>
				<a href="explore.php?content=potd" id="tabLink" onmouseover="menuOpen('dropDownMenu')" onmouseout="menuClose()">let's go exploring...</a>
				<div id="dropDownMenu" onmouseover="cancelClose('dropDownMenu')" onmouseout="menuClose()">
					<a href="explore.php?content=bbint">by interestingness</a>
					<a href="explore.php?content=bbrec">by most resent</a>
			        <a href="explore.php?content=bbcat">by category</a>
			        <a href="explore.php?content=bbspec">by spectrum</a>
			        <a href="explore.php?content=bbcotd">by colour of the day</a>
					<a href="explore.php?content=rand">randomly</a>
	        	</div>
			</li>
			<li>
				<a href="profile.php?username=<? echo $sessUsername; ?>" id="currentTabLink" onMouseOver="menuOpen('dropDownMenu2')" onMouseOut="menuClose()" >your profile</a>
				<div id="dropDownMenu2" onMouseOver="cancelClose('dropDownMenu2')" onMouseOut="menuClose()">
					<a href="userPhotos.php?username=<? echo $sessUsername;?>">your photos</a>
			        <a href="editAccount.php?userId=<? echo $sessUserId; ?>">adjust account</a>
	        	</div>
			</li>
			<li><a href="upload.php" id="tabLink">upload</a></li>
		</ul>

		<ul id="menu" class="floatRight">
			<?
			if($loggedIn){
				
				echo '<li><a href="logout.php" id="tabLink">logout '.$sessUsername.'</a></li>';
				if($sessUserType == 'a') {
					echo '<li><a href="admin.php" id="tabLink">admin</a></li>';
				}
			}
			else{
				echo '<li><a href="login.php" id="tabLink">login / sign up</a></li>';
			}
			?>
		</ul>
	</div>
	
	<!--MAIN-->
	<div id="main">
		<div id="floatLeft">
			<img src="photos/<? echo $userId."/".$avatar;?>" alt="<? echo $uesrname;?>'s avatar" width="50"/>
		</div>
		&nbsp <h2>Edit Account</h2><br />
		&nbsp Edit your account information here.<br /><br />
		
		<div id="large">
			<h2>Change Password</h2><br />
			<?
			if($passwordSet){
				echo "<span style=\"color: limegreen\">Your password has been changed successfully</span><br />";
			}
			?>
			<form name="changePassword" method="post" action="editAccount.php?userId=<? echo $userId; ?>" onSubmit="return passwordFormValidator(this)">
				<table>
					<tr>
						<td>Enter Current Password:</td><td><input type="password" name="oldPassword" onChange="checkOldPassword(this.value, <? echo $userId; ?>)" /> <span id="oldPasswordResult"></span></td>
					</tr>
					<tr>
						<td>Enter a new Password:</td><td><input type="password" name="password1" onChange="password1Validator(this.value)" /> <span id="passwordResult1"></span></td>
					</tr>
					<tr>
						<td>Re-Type Password:</td><td><input type="password" name="password2" onChange="password2Validator(password1.value,this.value)" /> <span id="passwordResult2"></span></td>
					</tr>
					<tr>
						<td><input type="submit" name="updatePassword" value="Update Password" class="button" /> 
					</tr>
				</table>
			</form>
			<div class="hr"></div>
			<br />
			
			<h2>Change Email</h2><br />
			<?
			if($emailSet){
				echo "<span style=\"color: limegreen\">Your email has been changed successfully</span><br />";
			}
			?>
			<form name="changeEmail" method="post" action="editAccount.php?userId=<? echo $userId; ?>" onSubmit="return emailFormValidator(this)">
				<table>
					<tr>
						<td>Email:</td><td><input type="text" name="email" onChange="emailValidator(this.value)" value="<? echo $email;?>" /> <span id="emailResult"></span></td>
					</tr>
					<tr>
						<td><input type="submit" name="updateEmail" value="Update Email" class="button" /> 
					</tr>
				</table>
			</form>
			<div class="hr"></div>
			<br />
			
			<h2>Change Other Information</h2><br />
			<?
			if($otherSet){
				echo "<span style=\"color: limegreen\">Your other informaiton has been changed successfully</span><br />";
			}
			?>
			<form name="changeOther" method="post" action="editAccount.php?userId=<? echo $userId; ?>" enctype="multipart/form-data">
				<table>
					<tr>
						<td>First Name:</td><td><input type="text" name="firstName" value="<? echo $firstName;?>" /></td>
					</tr>
					<tr>
						<td>Last Name:</td><td><input type="text" name="lastName" value="<? echo $lastName;?>" /></td>
					</tr>
					<tr>
						<td>About Yourself:</td><td><textarea name="otherInfo" cols="18" rows="5" ><? echo $otherInfo;?></textarea></td>
					</tr>
					<tr>
						<td>Avatar:</td><td><img src="photos/<? echo $userId."/".$avatar; ?>" width="100" /></td>
					</tr>
					<tr>
						<td></td><td><input type="file" name="avatar" id="file" /></td>
					</tr>
					<tr>
						<td><input type="submit" name="updateOther" value="Update Account Info" class="button" /> 
					</tr>
				</table>
			</form>
			<div class="hr"></div>
			<br />
			
			<h2>Delete Account</h2><br />
			<input type="submit" value="Delete Account" class="button" onClick="showHiddenDiv()" />
			<div id="hidden">
				<br />
				<span style="color: #FF0000">ARE YOU ABSOLUTELY SURE YOU WANT TO DO THIS!</span><br />
				I trust you are a responsible adult so I'm not going to give you another chance. When you delete your account, everything  to do with you is removed from our servers, this includes every single one of your pictures. If you do not have copies of your pictures yourself it is recommended that you go back to your account now and save all your full resolution images. Then if you absolutely must, come back here and delete your account.<br /><br />
				<form name="deleteAccount" method="post" action="editAccount.php?userId=<? echo $userId; ?>" >
					<input type="submit" name="DELETE" value="Yes I'm Sure, DELETE" class="button" />
				</form>
				<input type="submit" value="I don't know what I was thinking, nevermind" class="button" onClick="hideHiddenDiv()"/>
			</div>
		</div>
		<div id="small">
			<h2>Adjusting Your Info</h2>
			<div class="hr"></div>
			If you wish to change any aspect of your account you can do it here. The text fields are filled with your existing data, adjust it as need be and then submit using the buttons in the appropriate section.
		</div>
		<div id="clear"></div>
	</div>
	
	<!--FOOTER-->
	<ul id="menu">
		<li><a href="about.php" id="tabLink">about</a></li>
	</ul>
</div>
</body>

</html>