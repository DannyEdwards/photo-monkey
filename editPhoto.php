<?
/* 
* PHOTOMONKEY PHOTO EDIT PAGE
* EDITPHOTO.PHP
*
* Author: Daniel Edwards w1056952
*/

session_start();
include 'dbconnect.php';

//these lines of code assign dollar variables to the session values for use in the code.
$sessUsername = $_SESSION['username'];
$sessUserId = $_SESSION['userId'];
$sessUserType = $_SESSION['userType'];

//creates a single variable to deduce whether any user is a logged in member or not.
if($sessUsername && $sessUserId && $sessUserType) $loggedIn = true;
else $loggedIn = false;

if (isset($_POST['send'])){
	$photoId = $_POST['photoId'];
	$access = $_POST['access'];
	$title = $_POST['title'];
	$caption = $_POST['caption'];
	$category = $_POST['category'];
	$location = $_POST['location'];
	$tags = $_POST['tags'];
	$colour = $_POST['colour'];

	$query = "UPDATE photomonkey.photo SET photoAccess='$access', title='$title', category='$category', caption='$caption', location='$location', tags='$tags', colour='$colour' WHERE photoId='$photoId'";
	
	mysql_query($query);
	
	header("Location: photo.php?photoId=$photoId");
}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<!--head contains the .css file link, the favicon.ico link and the title-->
	<link rel="shortcut icon" href="images/favicon<? echo colourOfTheDay(); ?>.ico"/>
	<link rel="stylesheet" href="siteStyle.css"/>
	<script language="javascript" src="javascript.js"></script>
	<title>Edit Photo</title>
</head>

<body>
<div id="wrapper">
	<!--HEADER-->
	<div id="logo" onClick="location.href='index.php'">
		<img src="images/logo<? echo colourOfTheDay(); ?>.png" alt="logo image" title="Logo" width="50px" /> <h1>photomonkey</h1>
	</div>

	<div id="search">
		<form action="search.php?area=tags" method="post" >
				<input type="text" name="searchBox" class="input" value="Search..." onClick="this.value=''"/>
				<input type="submit" value="Go" class="button" />
		</form>
	</div>
	
	<div id="topNav">
		<ul id="menu">
			<li><a href="index.php" id="tabLink">home</a></li>
			<li>
				<a href="explore.php?content=potd" id="tabLink" onmouseover="menuOpen('dropDownMenu')" onmouseout="menuClose()">let's go exploring...</a>
				<div id="dropDownMenu" onmouseover="cancelClose('dropDownMenu')" onmouseout="menuClose()">
					<a href="explore.php?content=bbint">by interestingness</a>
					<a href="explore.php?content=bbrec">by most resent</a>
			        <a href="explore.php?content=bbcat">by category</a>
			        <a href="explore.php?content=bbspec">by spectrum</a>
			        <a href="explore.php?content=bbcotd">by colour of the day</a>
					<a href="explore.php?content=rand">randomly</a>
	        	</div>
			</li>
			<li>
				<a href="profile.php?username=<? echo $sessUsername; ?>" id="currentTabLink" onMouseOver="menuOpen('dropDownMenu2')" onMouseOut="menuClose()" >your profile</a>
				<div id="dropDownMenu2" onMouseOver="cancelClose('dropDownMenu2')" onMouseOut="menuClose()">
					<a href="userPhotos.php?username=<? echo $sessUsername;?>">your photos</a>
			        <a href="editAccount.php?userId=<? echo $sessUserId; ?>">adjust account</a>
	        	</div>
			</li>
			<li><a href="upload.php" id="tabLink">upload</a></li>
		</ul>

		<ul id="menu" class="floatRight">
			<?
			if($loggedIn){
				
				echo '<li><a href="logout.php" id="tabLink">logout '.$sessUsername.'</a></li>';
				if($sessUserType == 'a') {
					echo '<li><a href="admin.php" id="tabLink">admin</a></li>';
				}
			}
			else{
				echo '<li><a href="login.php" id="tabLink">login / sign up</a></li>';
			}
			?>
		</ul>
	</div>
	
	<!--MAIN-->
	<div id="main">
		<h2>Edit</h2><br />
		Edit your photos attributes.<br />
		<?
		$fileName = $_GET['fileName'];		

		$query = "SELECT photoId, ownerId, title, caption, category, location, tags, photoAccess, colour FROM photomonkey.photo WHERE fileName = '$fileName'";

		$result = mysql_query($query);

		$record = mysql_fetch_assoc($result);

		$photoId = $record['photoId'];
		$ownerId = $record['ownerId'];	
		$title = $record['title'];
		$caption = $record['caption'];
		$category = $record['category'];
		$tags = $record['tags'];
		$location = $record['location'];
		$photoAccess = $record['photoAccess'];
		$colour = $record['colour'];

		?>
		<div id="mediumLeft"><img src="<? echo "photos/$ownerId/scaled_$fileName"; ?>" alt="photo <? echo $title ?>" title="<? echo $title ?>" width="385" /></div>
		<div id="mediumRight">
			<h2>Adjust Attributes</h2><br />
			<form name="adjustAttributes" method="post">
			<table>
			<tr>
				<td>Title:</td><td><input type="text" name="title" class="input" value="<? echo $title; ?>" /></td>
			</tr>
			<tr>
				<td>Caption:</td><td><input type="text" name="caption" class="input" value="<? echo $caption; ?>" /></td>
			</tr>
			<tr>
				<td>Category:</td>
				<td>
				<select name="category">
					<option value="N">Category</option>
					<?
					if($category == "landscape") echo "<option value=\"landscape\" SELECTED>Landscape</option>";
					else echo "<option value=\"landscape\">Landscape</option>";

					if($category == "travel") echo "<option value=\"travel\" SELECTED>Travel</option>";
					else echo "<option value=\"travel\">Travel</option>";

					if($category == "cityscape") echo "<option value=\"cityscape\" SELECTED>Cityscape</option>";
					else echo "<option value=\"cityscape\">Cityscape</option>";

					if($category == "night") echo "<option value=\"night\" SELECTED>Night</option>";
					else echo "<option value=\"night\">Night</option>";

					if($category == "macro") echo "<option value=\"macro\" SELECTED>Macro</option>";
					else echo "<option value=\"macro\">Macro</option>";

					if($category == "panoramic") echo "<option value=\"panoramic\" SELECTED>Panoramic</option>";
					else echo "<option value=\"panoramic\">Panoramic</option>";

					if($category == "wildlife") echo "<option value=\"wildlife\" SELECTED>Wildlife</option>";
					else echo "<option value=\"wildlife\">Wildlife</option>";

					if($category == "nature") echo "<option value=\"nature\" SELECTED>Nature</option>";
					else echo "<option value=\"nature\">Nature</option>";

					if($category == "people") echo "<option value=\"people\" SELECTED>People</option>";
					else echo "<option value=\"people\">People</option>";

					if($category == "abstract") echo "<option value=\"abstract\" SELECTED>Abstract</option>";
					else echo "<option value=\"abstract\">Abstract</option>";
					?>
				</select>
				</td>
			</tr>
			<tr>
				<td>Tags:</td><td><input type="text" name="tags" class="input" value="<? echo $tags; ?>" /></td>
			</tr>
			<tr>
				<td>Location:</td><td><input type="text" name="location" class="input" value="<? echo $location; ?>" /></td>
			</tr>
			<tr>
				<td>Colour:</td>
				<td>
				<select name="colour">
					<option value="N">Colour</option>
					<?
					if($colour == "r") echo "<option value=\"r\" SELECTED>Red</option>";
					else echo "<option value=\"r\">Red</option>";

					if($colour == "o") echo "<option value=\"o\" SELECTED>Orange</option>";
					else echo "<option value=\"o\">Orange</option>";

					if($colour == "y") echo "<option value=\"y\" SELECTED>Yellow</option>";
					else echo "<option value=\"y\">Yellow</option>";

					if($colour == "g") echo "<option value=\"g\" SELECTED>Green</option>";
					else echo "<option value=\"g\">Green</option>";

					if($colour == "b") echo "<option value=\"b\" SELECTED>Blue</option>";
					else echo "<option value=\"b\">Blue</option>";

					if($colour == "i") echo "<option value=\"i\" SELECTED>Indigo</option>";
					else echo "<option value=\"i\">Indigo</option>";

					if($colour == "v") echo "<option value=\"v\" SELECTED>Violet</option>";
					else echo "<option value=\"v\">Violet</option>";
					?>
				</select>
				</td>
			</tr>
			<tr>
				<td>Access:</td>
				<td>
				<select name="access">
					<option value="o">Access</option>
					<?
					if($photoAccess == "o") echo "<option value=\"o\" SELECTED>Open</option>";
					else echo "<option value=\"o\">Open</option>";

					if($photoAccess == "s") echo "<option value=\"s\" SELECTED>Semi-Open</option>";
					else echo "<option value=\"s\">Semi-Open</option>";

					if($photoAccess == "r") echo "<option value=\"r\" SELECTED>Restricted</option>";
					else echo "<option value=\"r\">Restricted</option>";
					?>
				</select>
				</td>
			</tr>
			<tr>
				<td></td>
				<td>
					<input type="hidden" name="photoId" value="<? echo $photoId; ?>" />
					<input type="submit" name="send" value="Save" class="button" /> 
					<input type="reset" value="Clear" class="button" />
				</td>
			</tr>
			</table>
			</form>
		</div>
		<div id="clear"></div>
	</div>
	
	<!--FOOTER-->
	<ul id="menu">
		<li><a href="about.php" id="tabLink">about</a></li>
	</ul>
</div>
</body>

</html>