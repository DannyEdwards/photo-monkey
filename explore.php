<?
/* 
* PHOTOMONKEY EXPLORE PAGE
* EXPLORE.PHP
*
* Author: Daniel Edwards w1056952
*/

session_start();
include 'dbconnect.php';

//these lines of code assign dollar variables to the session values for use in the code.
$sessUsername = $_SESSION['username'];
$sessUserId = $_SESSION['userId'];
$sessUserType = $_SESSION['userType'];

//creates a single variable to deduce whether any user is a logged in member or not.
if($sessUsername && $sessUserId && $sessUserType) $loggedIn = true;
else $loggedIn = false;

$content = $_GET['content'];
//picture of the day by default.
if(!$content) $content = "potd";

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<!--head contains the .css file link, the favicon.ico link and the title-->
	<link rel="shortcut icon" href="images/favicon<? echo colourOfTheDay(); ?>.ico"/>
	<link rel="stylesheet" href="siteStyle.css"/>
	<script language="javascript" src="javascript.js"></script>
	<title>Explore</title>
</head>

<body>
<div id="wrapper">
	<!--HEADER-->
	<div id="logo" onClick="location.href='index.php'">
		<img src="images/logo<? echo colourOfTheDay(); ?>.png" alt="logo image" title="Logo" width="50px" /> <h1>photomonkey</h1>
	</div>

	<div id="search">
		<form action="search.php?area=tags" method="post" >
				<input type="text" name="searchBox" class="input" value="Search..." onClick="this.value=''"/>
				<input type="submit" value="Go" class="button" />
		</form>
	</div>
	
	<div id="topNav">
		<ul id="menu">
			<li><a href="index.php" id="tabLink">home</a></li>
			<li>
				<a href="explore.php?content=potd" id="currentTabLink" onmouseover="menuOpen('dropDownMenu')" onmouseout="menuClose()">let's go exploring...</a>
				<div id="dropDownMenu" onmouseover="cancelClose('dropDownMenu')" onmouseout="menuClose()">
					<a href="explore.php?content=bbint">by interestingness</a>
					<a href="explore.php?content=bbrec">by most resent</a>
			        <!--<a href="explore.php?content=bbcat">by category</a>-->
			        <a href="explore.php?content=bbspec">by spectrum</a>
			        <a href="explore.php?content=bbcotd">by colour of the day</a>
					<a href="explore.php?content=rand">randomly</a>
	        	</div>
			</li>
			<li>
				<a href="profile.php?username=<? echo $sessUsername; ?>" id="tabLink" onMouseOver="menuOpen('dropDownMenu2')" onMouseOut="menuClose()" >your profile</a>
				<div id="dropDownMenu2" onMouseOver="cancelClose('dropDownMenu2')" onMouseOut="menuClose()">
					<a href="userPhotos.php?username=<? echo $sessUsername;?>">your photos</a>
			        <a href="editAccount.php?userId=<? echo $sessUserId; ?>">adjust account</a>
	        	</div>
			</li>
			<li><a href="upload.php" id="tabLink">upload</a></li>
		</ul>

		<ul id="menu" class="floatRight">
			<?
			if($loggedIn){
				
				echo '<li><a href="logout.php" id="tabLink">logout '.$sessUsername.'</a></li>';
				if($sessUserType == 'a') {
					echo '<li><a href="admin.php" id="tabLink">admin</a></li>';
				}
			}
			else{
				echo '<li><a href="login.php" id="tabLink">login / sign up</a></li>';
			}
			?>
		</ul>
	</div>
	
	<!--MAIN-->
	<div id="main">
		<?

		if($content == "potd") {
			//picture of the day.
			$query = "SELECT photoId, ownerId, title, fileName FROM photomonkey.photo WHERE flagged = 'n' ORDER BY interestingness DESC LIMIT 0, 1;";
			$result = mysql_query($query);

			$record = mysql_fetch_assoc($result);

			$photoId = $record['photoId'];
			$ownerId = $record['ownerId'];
			$title = $record['title'];
			$fileName = $record['fileName'];
			
			$query = "SELECT username FROM photomonkey.user WHERE userId = '$ownerId'";
			$result = mysql_query($query);

			$record = mysql_fetch_assoc($result);
			$ownerUsername = $record['username'];

			//html section.
			?>
			<h2>"<? echo $title; ?>" is the Picture of the Day</h2><br />
			by <a href="profile.php?username=<? echo $ownerUsername ?>"><? echo $ownerUsername; ?></a><br />
			<div id="full">
				<div id="photo">
					<img src="<? echo "photos/$ownerId/scaled_$fileName"; ?>" alt="photo <? echo $title;?>" />
				</div>
			</div>
			<?
		}
		elseif($content == "bbcotd") {
			//browse by colour of the day.
			if(colourOfTheDay() == "R") {
				//red.
				$colourString = "Red";
				$query = "SELECT photoId, ownerId, fileName, colour FROM photomonkey.photo WHERE flagged = 'n' AND colour = 'r';";
				$result = mysql_query($query);
			}
			elseif(colourOfTheDay() == "O") {
				//orange.
				$colourString = "Orange";
				$query = "SELECT photoId, ownerId, fileName, colour FROM photomonkey.photo WHERE flagged = 'n' AND colour = 'o';";
				$result = mysql_query($query);
			}
			elseif(colourOfTheDay() == "Y") {
				//yellow.
				$colourString = "Yellow";
				$query = "SELECT photoId, ownerId, fileName, colour FROM photomonkey.photo WHERE flagged = 'n' AND colour = 'y';";
				$result = mysql_query($query);
			}
			elseif(colourOfTheDay() == "G") {
				//green.
				$colourString = "Green";
				$query = "SELECT photoId, ownerId, fileName, colour FROM photomonkey.photo WHERE flagged = 'n' AND colour = 'g';";
				$result = mysql_query($query);
			}
			elseif(colourOfTheDay() == "B") {
				//blue.
				$colourString = "Blue";
				$query = "SELECT photoId, ownerId, fileName, colour FROM photomonkey.photo WHERE flagged = 'n' AND colour = 'b';";
				$result = mysql_query($query);
			}
			elseif(colourOfTheDay() == "I") {
				//indigo.
				$colourString = "Indigo";
				$query = "SELECT photoId, ownerId, fileName, colour FROM photomonkey.photo WHERE flagged = 'n' AND colour = 'i';";
				$result = mysql_query($query);
			}
			elseif(colourOfTheDay() == "V") {
				//violet.
				$colourString = "Violet";
				$query = "SELECT photoId, ownerId, fileName, colour FROM photomonkey.photo WHERE flagged = 'n' AND colour = 'v';";
				$result = mysql_query($query);
			}
			
			echo "<h2>Browse by Colour of the Day</h2><br />";
			echo "Todays colour is $colourString";
			echo "<div id=\"full\">";
			echo "<table>";

			for ($cells = 0; $cells<5; $cells++){
				echo "<tr>";
				for($rows = 0; $rows<7; $rows++){

					$record = mysql_fetch_assoc($result);
					$fileName = $record['fileName'];
					$ownerId = $record['ownerId'];
					$photoId = $record['photoId'];
		
					if($record){
						echo "<td><a href=\"photo.php?photoId=$photoId\"><img src=\"photos/$ownerId/thumb_$fileName\" alt=\"photo $title\" title=\"$title\" width=110 border=0 /></a></td>";
					}
				}
				echo "</tr>";
			}
			echo "</table></div>";

		}
		elseif($content == "bbrec") {
			//browse by most rescent.
			//this is the query for fetching thumbnails for display organised by date added.
			$query = "SELECT photoId, ownerId, fileName FROM photomonkey.photo WHERE flagged = 'n' ORDER BY dateAdded DESC;";
			$result = mysql_query($query);

			echo "<h2>Browse by Most Rescent</h2><br />";
			echo "Fresh out of the dark room.";
			echo "<div id=\"full\">";
			echo "<table>";

			for ($cells = 0; $cells<5; $cells++){
				echo "<tr>";
				for($rows = 0; $rows<7; $rows++){

					$record = mysql_fetch_assoc($result);
					$fileName = $record['fileName'];
					$ownerId = $record['ownerId'];
					$photoId = $record['photoId'];
		
					echo "<td><a href=\"photo.php?photoId=$photoId\"><img src=\"photos/$ownerId/thumb_$fileName\" width=110 alt=\"photo $title\" title=\"$title\" border=0 /></a></td>";
				}
				echo "</tr>";
			}
			echo "</table></div>";
		}
		elseif($content == "bbint") {
			//browse by interestingness.
			//this is the query for fetching thumbnails for display organised by 'interestingness'.
			$query = "SELECT photoId, ownerId, fileName FROM photomonkey.photo WHERE flagged = 'n' ORDER BY interestingness DESC;";
			$result = mysql_query($query);

			echo "<h2>Browse by Interestingness</h2><br />";
			echo "True, interestingness is subjective, but we've used special maths to try and figure it out! I won't bore you with the details...";
			echo "<div id=\"full\">";
			echo "<table>";

			for ($cells = 0; $cells<5; $cells++){
				echo "<tr>";
				for($rows = 0; $rows<7; $rows++){

					$record = mysql_fetch_assoc($result);
					$fileName = $record['fileName'];
					$ownerId = $record['ownerId'];
					$photoId = $record['photoId'];
		
					echo "<td><a href=\"photo.php?photoId=$photoId\"><img src=\"photos/$ownerId/thumb_$fileName\" alt=\"photo $title\" title=\"$title\" width=110 border=0 /></a></td>";
				}
				echo "</tr>";
			}
			echo "</table></div>";
		}
		elseif($content == "bbspec") {
			//browse by spectrum.
			//for this a separate query is needed for each color so they can be arranged in the same screen.
			$redQuery = "SELECT photoId, ownerId, fileName, colour FROM photomonkey.photo WHERE flagged = 'n' AND colour = 'r' ORDER BY RAND();";
			$orangeQuery = "SELECT photoId, ownerId, fileName, colour FROM photomonkey.photo WHERE flagged = 'n' AND colour = 'o' ORDER BY RAND();";
			$yellowQuery = "SELECT photoId, ownerId, fileName, colour FROM photomonkey.photo WHERE flagged = 'n' AND colour = 'y' ORDER BY RAND();";
			$greenQuery = "SELECT photoId, ownerId, fileName, colour FROM photomonkey.photo WHERE flagged = 'n' AND colour = 'g' ORDER BY RAND();";
			$blueQuery = "SELECT photoId, ownerId, fileName, colour FROM photomonkey.photo WHERE flagged = 'n' AND colour = 'b' ORDER BY RAND();";
			$indigoQuery = "SELECT photoId, ownerId, fileName, colour FROM photomonkey.photo WHERE flagged = 'n' AND colour = 'i' ORDER BY RAND();";
			$violetQuery = "SELECT photoId, ownerId, fileName, colour FROM photomonkey.photo WHERE flagged = 'n' AND colour = 'v' ORDER BY RAND();";

			$redResult = mysql_query($redQuery);
			$orangeResult = mysql_query($orangeQuery);
			$yellowResult = mysql_query($yellowQuery);
			$greenResult = mysql_query($greenQuery);
			$blueResult = mysql_query($blueQuery);
			$indigoResult = mysql_query($indigoQuery);
			$violetResult = mysql_query($violetQuery);

			echo "<h2>Browse by the Spectrum</h2><br />";
			echo "All the colours of the... electromagnetic spectrums' visible range!";
			echo "<div id=\"full\">";
			echo "<table>";

			for ($cells = 0; $cells<5; $cells++){
				echo "<tr>";
				for($rows = 0; $rows<7; $rows++){
					
					if($cells == 0 && $rows == 0)echo "<th>Red</th>";
					elseif($cells == 0 && $rows == 1)echo "<th>Orange</th>";
					elseif($cells == 0 && $rows == 2)echo "<th>Yellow</th>";
					elseif($cells == 0 && $rows == 3)echo "<th>Green</th>";
					elseif($cells == 0 && $rows == 4)echo "<th>Blue</th>";
					elseif($cells == 0 && $rows == 5)echo "<th>Indigo</th>";
					elseif($cells == 0 && $rows == 6)echo "<th>Violet</th>";
					else{
						if($rows == 0) $record = mysql_fetch_assoc($redResult);
						elseif($rows == 1) $record = mysql_fetch_assoc($orangeResult);
						elseif($rows == 2) $record = mysql_fetch_assoc($yellowResult);
						elseif($rows == 3) $record = mysql_fetch_assoc($greenResult);
						elseif($rows == 4) $record = mysql_fetch_assoc($blueResult);
						elseif($rows == 5) $record = mysql_fetch_assoc($indigoResult);
						elseif($rows == 6) $record = mysql_fetch_assoc($violetResult);

						$fileName = $record['fileName'];
						$ownerId = $record['ownerId'];
						$photoId = $record['photoId'];
						
						if($record){
							echo "<td><a href=\"photo.php?photoId=$photoId\"><img src=\"photos/$ownerId/thumb_$fileName\" alt=\"photo $title\" title=\"$title\" width=110 border=0 /></a></td>";
						}
						else echo "<td></td>";
					}
					
				}
				echo "</tr>";
			}
			echo "</table></div>";
		}
		elseif($content == "bbcat") {
			//browse by category.
			echo "<h2>Browse by Category</h2><br />";
		}
		elseif($content == "rand") {
			//browse by randomness.
			//this is the query for fetching thumbnails for display organised by 'interestingness'.
			$query = "SELECT photoId, ownerId, fileName FROM photomonkey.photo WHERE flagged = 'n' ORDER BY RAND();";
			$result = mysql_query($query);

			echo "<h2>Random Browsing</h2><br />";
			echo "Some random pictures for you to explore willy nilly.";
			echo "<div id=\"full\">";
			echo "<table>";

			for ($cells = 0; $cells<5; $cells++){
				echo "<tr>";
				for($rows = 0; $rows<7; $rows++){

					$record = mysql_fetch_assoc($result);
					$fileName = $record['fileName'];
					$ownerId = $record['ownerId'];
					$photoId = $record['photoId'];
		
					echo "<td><a href=\"photo.php?photoId=$photoId\"><img src=\"photos/$ownerId/thumb_$fileName\" alt=\"photo $title\" title=\"$title\" width=110 border=0 /></a></td>";
				}
				echo "</tr>";
			}
			echo "</table></div>";
		}

		else{
			header("Location: explore.php?content=potd");
		}
		?>
			<br /><h2>Where Shall We Go To Explore?</h2><br />
			<h3><a href="explore.php?content=bbrec">Browse by Most Recent</a> | 
			<a href="explore.php?content=bbspec">Browse by the Spectrum</a> | 
			<!--<a href="explore.php?content=bbcat">Browse by Category</a> | -->
			<a href="explore.php?content=potd">See the Picture of the Day</a> | 
			<a href="explore.php?content=bbcotd">Browse by Colour of the Day</a></h3><br />
	</div><!--end of "main" div-->
	
	<!--FOOTER-->
	<ul id="menu">
		<li><a href="about.php" id="tabLink">about</a></li>
	</ul>
</div>
</body>

</html>