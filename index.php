<?
/* 
* PHOTOMONKEY INDEX PAGE
* INDEX.PHP
*
* Author: Daniel Edwards w1056952
*/

session_start();
include 'dbconnect.php';

//assigns dollar variables to the session values for use in the code.
$sessUsername = $_SESSION['username'];
$sessUserId = $_SESSION['userId'];
$sessUserType = $_SESSION['userType'];

//creates a single variable to deduce whether any user is a logged in member or not.
if($sessUsername && $sessUserId && $sessUserType) $loggedIn = true;
else $loggedIn = false;

//this is the query for fetching thumbnails for display organised by 'interestingness'.
$query =	"SELECT photoId, ownerId, fileName, title
			FROM photomonkey.photo
			WHERE flagged = 'n'
			ORDER BY RAND();";
$result = mysql_query($query);

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<!--head contains the .css file link, the favicon.ico link and the title-->
	<link rel="shortcut icon" href="images/favicon<? echo  colourOfTheDay(); ?>.ico"/>
	<link rel="stylesheet" href="siteStyle.css"/>
	<script language="javascript" src="javascript.js"></script>
	<title>Welcome</title>
</head>

<body>
<div id="wrapper">
	<!--HEADER-->

	<div id="logo" onClick="index.php">
		<img src="images/logo<? echo colourOfTheDay(); ?>.png" alt="logo image" title="Logo" width="50px" /> <h1>photomonkey</h1>
	</div>

	<div id="search">
		<form action="search.php?area=tags" method="post" >
			<input type="text" name="searchBox" class="input" value="Search..." onClick="this.value=''"/>
			<input type="submit" value="Go" class="button" />
		</form>
	</div>

	<div id="topNav">
		<ul id="menu">
			<li><a href="index.php" id="currentTabLink">home</a></li>
			<li>
				<a href="explore.php?content=potd" id="tabLink" onmouseover="menuOpen('dropDownMenu')" onmouseout="menuClose()">let's go exploring...</a>
				<div id="dropDownMenu" onmouseover="cancelClose('dropDownMenu')" onmouseout="menuClose()">
					<a href="explore.php?content=bbint">by interestingness</a>
					<a href="explore.php?content=bbrec">by most resent</a>
			        <!--<a href="explore.php?content=bbcat">by category</a>-->
			        <a href="explore.php?content=bbspec">by spectrum</a>
			        <a href="explore.php?content=bbcotd">by colour of the day</a>
					<a href="explore.php?content=rand">randomly</a>
	        	</div>
			</li>
			<li>
				<a href="profile.php?username=<? echo $sessUsername; ?>" id="tabLink" onMouseOver="menuOpen('dropDownMenu2')" onMouseOut="menuClose()" >your profile</a>
				<div id="dropDownMenu2" onMouseOver="cancelClose('dropDownMenu2')" onMouseOut="menuClose()">
					<a href="userPhotos.php?username=<? echo $sessUsername;?>">your photos</a>
			        <a href="editAccount.php?userId=<? echo $sessUserId; ?>">adjust account</a>
	        	</div>
			</li>
			<li><a href="upload.php" id="tabLink">upload</a></li>
		</ul>

		<ul id="menu" class="floatRight">
			<?
			if($loggedIn){
				
				echo '<li><a href="logout.php" id="tabLink">logout '.$sessUsername.'</a></li>';
				if($sessUserType == 'a') {
					echo '<li><a href="admin.php" id="tabLink">admin</a></li>';
				}
			}
			else{
				echo '<li><a href="login.php" id="tabLink">login / sign up</a></li>';
			}
			?>
		</ul>
	</div>
	
	<!--MAIN-->
	<div id="main">
		<h2>Alright?</h2><br />
		Have a look at some of the photos our users are uploading...<br />
		<div id="full">
		<?
		echo "<table>";

		for ($rows = 0; $rows<5; $rows++){
			echo "<tr>";
			for($columns = 0; $columns<7; $columns++){

				$record = mysql_fetch_assoc($result);
				$fileName = $record['fileName'];
				$ownerId = $record['ownerId'];
				$photoId = $record['photoId'];
				$title = $record['title'];
		
				echo "<td><a href=\"photo.php?photoId=$photoId\"><img src=\"photos/$ownerId/thumb_$fileName\" alt=\"thumbnail of $title\" title=\"$title\" width=110 border=0 /></a></td>";
			}
			echo "</tr>";
		}

		echo "</table>";
		?>
		<a href="index.php">Ransomise!</a>
		</div>
	</div>
	
	<!--FOOTER-->
	<ul id="menu">
		<li><a href="about.php" id="tabLink">about</a></li>
	</ul>
</div>
</body>
</html>