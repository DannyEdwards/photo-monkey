/* 
* PHOTOMONKEY JAVASCRIPT FUNCTIONS
* JAVASCRIPT.JS
*
* Author: Daniel Edwards w1056952
*/

//***********************CODE CONCERNING THE DROPDOWN MENU***********************\\
var menuItem = 0;

// open hidden layer
function menuOpen(id) {	
	// close old layer
	if(menuItem) menuItem.style.visibility = 'hidden';

	// get new layer and show it
	menuItem = document.getElementById(id);
	menuItem.style.visibility = 'visible';
}

// close showed layer
function menuClose() {
	if(menuItem) menuItem.style.visibility = 'hidden';
}

// cancel close timer
function cancelClose(id) {

	// get new layer and show it
	menuItem = document.getElementById(id);
	menuItem.style.visibility = 'visible';
}

// close layer when click-out
document.onclick = menuClose();

//***************************CODE CONCERNING PHOTO.PHP***************************\\
function submitRatingAndFaveForm() {
	document.ratingAndFaveForm.favourite.value = "set";
	document.ratingAndFaveForm.submit();
}

function submitForm(formId) {
	document.getElementById(formId).submit();
}

function showComment(comment) {
	document.getElementById(comment).style.visibility = 'visible';
}

function hideComment(comment) {
	document.getElementById(comment).style.visibility = 'hidden';
}

//***************************CODE CONCERNING LOGIN.PHP***************************\\
function submitLoginForm(){
	document.login.action.value = "forgottenPassword";
	document.login.submit();
}

//xmlHttp variable
var xmlHttp

//this function is used to call the xmlhttpobject.
function GetXmlHttpObject(){
	//sets objXMLHttp to null by default.
	var objXMLHttp=null;
	
	if (window.XMLHttpRequest){
		//if Netscape or any other browser than IE is detected, use xmlhttp.
		objXMLHttp=new XMLHttpRequest(); //creates a xmlhttp request.
	}
	else if (window.ActiveXObject){
		//elseIf IE is being used, use IE Active X.
		objXMLHttp=new ActiveXObject("Microsoft.XMLHTTP"); //creates a new Active X Object.
	}
	
	//returns the xhttp object.
	return objXMLHttp;
}

var usernameValid = false;

//this function we will use to check to see if a username is available or not.
function checkSignUpUsername(username){
	//creates a new Xmlhttp object.
	xmlHttp=GetXmlHttpObject();
	
	//cannot create a new Xmlhttp object.
	if (xmlHttp==null){
		alert ("Browser does not support HTTP Request");
		return;
	}

	if(username == "" || username == null){
		//if the username is empty, then this message is displayed.
		document.getElementById("signUpUsernameResult").innerHTML = '<span style="color:red"> This needs to be filled in.</style>';
		usernameValid = false;
	}
	else {
		//if the username is not empty then the xmlHTTPrequest can continue.
		var url="checkusername.php?request=checkUsername&username="+username;
	
		xmlHttp.open("GET",url,true); //opens the URL using GET.
	
		xmlHttp.onreadystatechange = function() {
			if (xmlHttp.readyState == 4) {
				if(xmlHttp.responseText == 'true'){
					document.getElementById("signUpUsernameResult").innerHTML = '<span style="color:red"> This username is taken.</style>';
					usernameValid = false;
				}
				else if(xmlHttp.responseText == 'false'){
					document.getElementById("signUpUsernameResult").innerHTML = '<span style="color:limegreen"> This username is available.</style>';
					usernameValid = true;
				}
			}
		};
	}
	//sends NULL instead of sending data.
	xmlHttp.send(null);
}

function xmlHttpValidator(){
	document.getElementById("signUpUsernameResult")
}

function checkLoginUsername(username){
	//creates a new Xmlhttp object.
	xmlHttp=GetXmlHttpObject();
	
	//cannot create a new Xmlhttp object.
	if (xmlHttp==null){
		alert ("Browser does not support HTTP Request");
		return;
	}

	if(username == "" || username == null){
		document.getElementById("loginUsernameResult").innerHTML = '<span style="color:red"> This needs to be filled in.</style>';
	}
	else {
		var url="checkusername.php?request=checkUsername&username="+username;
	
		xmlHttp.open("GET",url,true); //opens the URL using GET.
	
		xmlHttp.onreadystatechange = function() {
			if (xmlHttp.readyState == 4) {
				if(xmlHttp.responseText == 'true'){
					document.getElementById("loginUsernameResult").innerHTML = '<span style="color:limegreen"> Welcome Back!</style>';
				}
				else if(xmlHttp.responseText == 'false'){
					document.getElementById("loginUsernameResult").innerHTML = '<span style="color:red"> This username does not exist.</style>';
				}
				//end my alternate code
			}
		};
	}
	//sends NULL instead of sending data.
	xmlHttp.send(null);
}

//in depth client-side javascript form validation follows.

//this checks the validity of the password entered into the field.
//it also includes a simple password strength check.
function password1Validator(password){
	//these variables are used to check whether they are present in the password field.
	var alpha = /^[a-zA-Z]+$/;
	var numeric = /^[0-9]+$/;

	if(password == "" || password == null){
		document.getElementById("passwordResult1").innerHTML = '<span style="color:red">You need to fill this in.</span>';
		return(false)
	}
	else if(password.length < 8){
		//if the password is under 8 chars, it is deemed insfficient and the user cannot continue.
		document.getElementById("passwordResult1").innerHTML = '<span style="color:red">Minimum 8 characters.</span>';
		return(false)
		
	}
	else if(password.match(alpha) && !password.match(numeric)){
		//if the password is all letters but no numbers the user may submit, but they are advides that the password may not be very strong.
		document.getElementById("passwordResult1").innerHTML = '<span style="color:yellow">OK. (Try including numbers)</span>';
		return (true)
	}
	else if(!password.match(alpha) && password.match(numeric)){
		//this deals with a totaly numerical passwords and leaves a cheeky comment!
		document.getElementById("passwordResult1").innerHTML = '<span style="color:limegreen">Ooh, hardcore!</span>';
		return (true)
	}
	else{
		//this else would deal with any alphanumerical passwords... perfect!
		document.getElementById("passwordResult1").innerHTML = '<span style="color:limegreen">Perfect.</span>';
		return(true)
	}
}

//this function checks that the 2nd password field is not blank and it matches the 1st.
function password2Validator(password1,password2){
	if(password2 == ""){
		document.getElementById("passwordResult2").innerHTML = '<span style="color:red">This needs to be filled in.</span>';
		return(false)
	}
	else if(password1 != password2){
		document.getElementById("passwordResult2").innerHTML = '<span style="color:red">Passwords don\'t match.</span>';
		return(false)
	}
	else{
		document.getElementById("passwordResult2").innerHTML = '<span style="color:limegreen">Next.</span>';
		return(true)
	}
}

//this function checks the validity of the email.
function emailValidator(email){
	//this bit of code is a simple email validation, it ensures an "@" and full stop are present.
	//first i find the index point of an "@" and a "." character.
	at=email.indexOf("@")
	dot=email.indexOf(".")

	//if the symbols are not found, there default value of -1 will persist.
	if(at == -1 || dot == -1){
		document.getElementById("emailResult").innerHTML = '<span style="color:red">Not a valid email.</span>';
		return(false)
	}
	else{
		document.getElementById("emailResult").innerHTML = '<span style="color:limegreen">Lovely email.</span>';
		return(true)
	}
}

//this one is just a bit of fun.
function commentsComment(comments){
	if(comments.length > 100){
		document.getElementById("commentsResult").innerHTML = '<span style="color:limegreen">Alright, alright! Let\'s get on with this shall we?</span>';
	}
}

//this checks whether the user has agreed with the terms and conditions.
function termsValidator(terms){
	if(terms.checked == true){
		document.getElementById("termsResult").innerHTML = '<span style="color:limegreen">Cheers.</span>';
		return(true)
	}
	else{
		document.getElementById("termsResult").innerHTML = '<span style="color:red">You have to agree with these.</span>';
		return(false)
	}
}

//the final function checks if all validation is complete, if so, the form can be submitted.
function formValidator(signUp){
	if(usernameValid==true && password1Validator(document.signUp.password1.value)==true && password2Validator(document.signUp.password1.value,document.signUp.password2.value)==true &&  emailValidator(document.signUp.email.value)==true && termsValidator(document.signUp.terms)==true){
		return(true)
	}
	else{
		document.getElementById("signUpUsernameResult").innerHTML = '<span style="color:red">There is a problem here.</style>';
		return(false)
	}
}

//*************************CODE CONCERNING EDITACCOUNT.PHP*************************\\
function showHiddenDiv(){
	document.getElementById("hidden").style.visibility = 'visible';
	document.getElementById("hidden").style.display = 'block';
}

function hideHiddenDiv(){
	document.getElementById("hidden").style.visibility = 'hidden';
	document.getElementById("hidden").style.display = 'none';
}

function emailFormValidator(form){
	if(emailValidator(document.changeEmail.email.value)==true){
		return(true)
	}
	else{
		return(false)
	}
}

function passwordFormValidator(form){
	if(passwordValid == true && password1Validator(document.changePassword.password1.value)==true && password2Validator(document.changePassword.password1.value,document.changePassword.password2.value)==true){
		return(true)
	}
	else{
		return(false)
	}
}

var passwordValid = false;

function checkOldPassword(password, userId){
	//creates a new Xmlhttp object.
	xmlHttp=GetXmlHttpObject();
	
	//cannot create a new Xmlhttp object.
	if (xmlHttp==null){
		alert ("Browser does not support HTTP Request");
		return;
	}

	if(password == "" || password == null){
		document.getElementById("oldPasswordResult").innerHTML = '<span style="color:red"> This needs to be filled in.</style>';
		passwordValid = false;
	}
	else {
		var url="checkusername.php?request=checkOldPassword&password="+password+"&userId="+userId;
	
		xmlHttp.open("GET",url,true); //opens the URL using GET.
	
		xmlHttp.onreadystatechange = function() {
			if (xmlHttp.readyState == 4) {
				if(xmlHttp.responseText == 'true'){
					document.getElementById("oldPasswordResult").innerHTML = '<span style="color:limegreen">Correct</style>';
					passwordValid = true;
				}
				else if(xmlHttp.responseText == 'false'){
					document.getElementById("oldPasswordResult").innerHTML = '<span style="color:red">This is not your password.</style>';
					passwordValid = false;
				}
				//end my alternate code
			}
		};
	}
	//sends NULL instead of sending data.
	xmlHttp.send(null);
}

//*************************CODE CONCERNING UPLOAD.PHP*************************\\
function fileUploadFeedback(spanId){
	document.getElementById(spanId).innerHTML = '<span style="color:limegreen">Complete</style>'
}