<?
/* 
* PHOTOMONKEY LOGOUT
* LOGOUT.PHP
*
* Author: Daniel Edwards w1056952
*/

//this file destroys the session and any cookies.
session_start();
session_destroy();
if(isset($_COOKIE['userId']) && isset($_COOKIE['username']) && isset($_COOKIE['userType'])){
	setcookie("userId","",-1,"/");
	setcookie("username","",-1,"/");
	setcookie("userType","",-1,"/");
}
header("Location: index.php"); 
?>