<?
/* 
* PHOTOMONKEY LOGIN AND SIGN UP PAGE
* LOGIN.PHP
*
* Author: Daniel Edwards w1056952
*/

session_start();
include 'dbconnect.php';

if(isset($_SESSION['userId']) && isset($_SESSION['username'])){
	$username = $_SESSION['username'];
	
	header("Location: profile.php?username=$username");
}

//this variable is present if the user has been redirected to the login page from elsewhere.
$redirection = $_GET['redirection'];

if (isset($_POST['loginUsername']) && isset($_POST['password']) && $_POST['action'] == "login"){	

	$formUsername = $_POST['loginUsername'];
	$formPassword = crypt($_POST['password'], "pw");
    
    $query =	"SELECT COUNT(*), userId, username, password, userType
				FROM photomonkey.user
				WHERE username='$formUsername'
				AND password='$formPassword'";
	$result = mysql_query($query);
	$record = mysql_fetch_assoc($result);
	
	$dbPassword = $record['password'];
	$dbUserId = $record['userId'];
	$dbUsername = $record['username'];
	$dbUserType = $record['userType'];
	
	if($record['COUNT(*)'] > 0 && $dbPassword == $formPassword){
		
		//the session is adapted using the users unique identifyers.
		$_SESSION['userId']= $dbUserId;  
		$_SESSION['username'] = $dbUsername;
		$_SESSION['userType'] = $dbUserType;
		
		//the users lastLogin in attribute is updated to the current time.
		$query =	"UPDATE photomonkey.user
					SET lastLogin = NOW()
					WHERE userId = '$dbUserId'";
		mysql_query($query);
		
		//sets a cookie without expiry until 30 days if rememberMe is checked.
		if(isset($_POST['rememberMe'])){
			setcookie("userId", $_SESSION['userId'], time()+60*60*24*30, "/");
			setcookie("username", $_SESSION['username'], time()+60*60*24*30, "/");
			setcookie("userType", $_SESSION['userType'], time()+60*60*24*30, "/");
		}
		
		header("Location: profile.php?username=$dbUsername"); 
	}
	else{
		header("Location: login.php?&redirection=loginFail"); 
	}
	
}
else if ($_POST['action'] == "forgottenPassword"){
	//log in to root.
	//sudo g-w /
	//start sendmail.
	//sudo /usr/sbin/sendmail -bd -q1m


	if($_POST['loginUsername'] == ""){
		$fillUsername = true;
	}
	else {
		$newPassword = substr(md5(rand()), 0, 8);

		$loginUsername = $_POST['loginUsername'];
		$query =	"SELECT email
					FROM user
					WHERE username = '$loginUsername'";
		$result = mysql_query($query);
		$record = mysql_fetch_assoc($result);
		
		$email = $record['email'];
		$subject = "Your new password.";
		$body = "<html>Ever so sorry to hear you lost your password, so here is a new one:<br /><br />
				<b>$newPassword</b><br /><br />
				Your old password will no longer work. Use this new password to log into your account and reset your password to something you can remember from there.</html>";

		$header  = 'MIME-Version: 1.0' . "\r\n";
		$header .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
		$header .= 'From: photomonkey' . "\r\n";

		//mail($email, $subject, $body, $header);

		if(mail($email, $subject, $body, $header)) $mailSent = "success";
		else $mailSent = "fail";

		$query = "UPDATE photomonkey.user 
				SET password = '$newPassword'
				WHERE email = '$email'";
		
		//mysql_query($query);
	}
}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<!--head contains the .css file link, the favicon.ico link and the title-->
	<link rel="shortcut icon" href="images/favicon<? echo colourOfTheDay(); ?>.ico"/>
	<link rel="stylesheet" href="siteStyle.css"/>
	<script language="javascript" src="javascript.js"></script>
	<title>Log In</title>
</head>

<body>
<div id="wrapper">
	<!--HEADER-->
	<div id="logo" onClick="location.href='index.php'">
		<img src="images/logo<? echo colourOfTheDay(); ?>.png" alt="logo image" title="Logo" width="50px" /> <h1>photomonkey</h1>
	</div>

	<div id="search">
		<form action="search.php?area=tags" method="post" >
				<input type="text" name="searchBox" class="input" value="Search..." onClick="this.value=''"/>
				<input type="submit" value="Go" class="button" />
		</form>
	</div>
	
	<div id="topNav">
		<ul id="menu">
			<li><a href="index.php" id="tabLink">home</a></li>
			<li>
				<a href="explore.php?content=potd" id="tabLink" onmouseover="menuOpen('dropDownMenu')" onmouseout="menuClose()">let's go exploring...</a>
				<div id="dropDownMenu" onmouseover="cancelClose('dropDownMenu')" onmouseout="menuClose()">
					<a href="explore.php?content=bbint">by interestingness</a>
					<a href="explore.php?content=bbrec">by most resent</a>
			        <a href="explore.php?content=bbcat">by category</a>
			        <a href="explore.php?content=bbspec">by spectrum</a>
			        <a href="explore.php?content=bbcotd">by colour of the day</a>
					<a href="explore.php?content=rand">randomly</a>
	        	</div>
			</li>
			<li>
				<a href="profile.php?username=<? echo $sessUsername; ?>" id="tabLink" onMouseOver="menuOpen('dropDownMenu2')" onMouseOut="menuClose()" >your profile</a>
				<div id="dropDownMenu2" onMouseOver="cancelClose('dropDownMenu2')" onMouseOut="menuClose()">
					<a href="userPhotos.php?username=<? echo $sessUsername;?>">your photos</a>
			        <a href="editAccount.php?userId=<? echo $sessUserId; ?>">adjust account</a>
	        	</div>
			</li>
			<li><a href="upload.php" id="tabLink">upload</a></li>
		</ul>

		<ul id="menu" class="floatRight">
			<li><a href="login.php" id="currentTabLink">login / sign up</a></li>
		</ul>
	</div>
	
	<!--MAIN-->
	<div id="main">
		<h2>Login or Sign Up</h2><br />
		<div id="large">
			<h2>Login</h2> <!--Login Section-->
			<form name="login" id="loginForm" action="login.php" method="post">
				<table>
					<tr>
						<td>Username:</td><td><input type="text" name="loginUsername" class="input" onChange="checkLoginUsername(this.value)" /> <span id="loginUsernameResult"></span></td>
					</tr>
					<tr>
						<td>Password:</td><td><input type="password" name="password" class="input" /></td>
					</tr>
					<tr>
						<td><input name="rememberMe" type="checkbox">Remember me</td>
					</tr>
					<tr>
						<td><input type="submit" value="login" class="button"></td>
					</tr>
					<tr>
						<td><a href="javascript:submitLoginForm()">Forgotten your password?</a>
						<input type="hidden" name="action" value="login" /></td>
					</tr>
					<?
					if($fillUsername) {
						echo "<tr><td colspan=\"2\">";
						echo "<span style=\"color:red\">You need to fill in the username field first.</span>";
						echo "</td></tr>";
					}
					else if($mailSent == "success") {
						echo "<tr><td colspan=\"2\">";
						echo "<span style=\"color:green\">A new password has been sent to your email.</span>";
						echo "</td></tr>";
					}
					else if($mailSent == "fail") {
						echo "<tr><td colspan=\"2\">";
						echo "<span style=\"color:red\">There was a problem sending you an email, hold on...</span>";
						echo "</td></tr>";
					}
					?>
				</table>
			</form>
			<div class="hr"></div>
			<h2>Sign Up</h2> <!--Sign Up Section-->
			<form name="signUp" action="signUp.php" method="post" enctype="multipart/form-data" onSubmit="return formValidator(this)" >
				<table>
					<tr>
						<td>Enter a Username:</td><td><input type="text" name="signUpUsername" onBlur="checkSignUpUsername(this.value)" /> * <span id="signUpUsernameResult"></span></td>
					</tr>
					<tr>
						<td>Enter a Password:</td><td><input type="password" name="password1" onChange="password1Validator(this.value)" /> * <span id="passwordResult1"></span></td>
					</tr>
					<tr>
						<td>Re-Type Password:</td><td><input type="password" name="password2" onChange="password2Validator(password1.value,this.value)" /> * <span id="passwordResult2"></span></td>
					</tr>
					<tr>
						<td>Email:</td><td><input type="text" name="email" onChange="emailValidator(this.value)" /> * <span id="emailResult"></span></td>
					</tr>
					<tr>
						<td>First Name:</td><td><input type="text" name="firstName" /></td>
					</tr>
					<tr>
						<td>Last Name:</td><td><input type="text" name="lastName" /></td>
					</tr>
					<tr>
						<td>About Yourself:</td><td><textarea name="comments" cols="18" rows="5" onkeypress="commentsComment(this.value)"></textarea> <span id="commentsResult"></span></td>
					</tr>
					<tr>
						<td>Avatar:</td><td><input type="file" name="avatar" id="file" /></td>
					</tr>
					<tr>
						<td colspan="2"><input type="checkbox" name="terms" onclick="termsValidator(this)" /> Yes, I agree with the <a href="about.php?area=terms">terms and conditions</a>.* <span id="termsResult"></span></td>
					</tr>
					<tr>
						<td><input type="submit" name="send" value="sign me up!" class="button" /> 
						<input type="reset" value="clear" class="button" /></td>
					</tr>
					<tr>
						<td height="50px" colspan="2">* These fields are required.</td>
					</tr>
				</table>
			</form>
		</div>
		<div id="small">
			<?
			if ($redirection == "loginFail"){
				echo "<h2>Oops</h2>";
				echo "<div class=\"hr\"></div>";
				echo "Your last login failed, the password was incorrect. If you are not a member, sign up!<br />";
				echo "<br />";
			}
			else if ($redirection == "restrictedContent"){
				echo "<h2>Sorry</h2>";
				echo "<div class=\"hr\"></div>";
				echo "The content you are trying to access is available to members only. Login on the left or Sign Up below. Becoming a member is, of course, free and easy. Do it now!<br />";
				echo "<br />";
			}
			else if ($redirection == "signUpFail"){
				echo "<h2>Oops</h2>";
				echo "<div class=\"hr\"></div>";
				echo "You have not filled in all of the required fields in order to be able to sign up.<br />";
				echo "<br />";
			}
			?>
			<h2>Why Sign Up?</h2>
			<div class="hr"></div>
			Photomonkey is a photo sharing website for people with a passion for photography, no matter what your skill. A platform from which photographers can share their work with each other and gain inspiration from other users.<br />
			Photomonkey is, of course, absolutely free to join and signing up wont take a minute. Sign up here, now!
		</div>
		<div id="clear"></div>
	</div>

	<!--FOOTER-->
	<ul id="menu">
		<li><a href="about.php" id="tabLink">about</a></li>
	</ul>
</div>
</body>

</html>