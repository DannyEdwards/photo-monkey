--
-- Database: `photomonkey`
--

-- --------------------------------------------------------

--
-- Table structure for table `album`
--

CREATE TABLE IF NOT EXISTS `album` (
  `albumId` int(11) NOT NULL AUTO_INCREMENT,
  `ownerId` int(11) NOT NULL,
  `name` varchar(30) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `caption` varchar(50) DEFAULT NULL,
  `category` varchar(20) DEFAULT NULL,
  `tags` varchar(50) DEFAULT NULL,
  `location` varchar(20) DEFAULT NULL,
  `noRatings` int(3) DEFAULT '0',
  `cumulativeRatings` int(4) DEFAULT '0',
  `rating` int(1) DEFAULT NULL,
  `views` int(11) DEFAULT '0',
  `comments` int(11) DEFAULT '0',
  `interestingness` int(11) DEFAULT '0',
  PRIMARY KEY (`albumId`),
  KEY `ownerId` (`ownerId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `albumConn`
--

CREATE TABLE IF NOT EXISTS `albumConn` (
  `photoId` int(11) NOT NULL,
  `albumId` int(11) NOT NULL,
  PRIMARY KEY (`photoId`,`albumId`),
  KEY `albumId` (`albumId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `comment`
--

CREATE TABLE IF NOT EXISTS `comment` (
  `userId` int(11) NOT NULL,
  `photoId` int(11) NOT NULL,
  `comment` varchar(500) DEFAULT NULL,
  `datePosted` datetime NOT NULL,
  `flagged` enum('y','n') DEFAULT 'n',
  PRIMARY KEY (`userId`,`photoId`,`datePosted`),
  KEY `photoId` (`photoId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `favourite`
--

CREATE TABLE IF NOT EXISTS `favourite` (
  `userId` int(11) NOT NULL,
  `photoId` int(11) NOT NULL,
  `dateFavourited` datetime NOT NULL,
  PRIMARY KEY (`userId`,`photoId`),
  KEY `photoId` (`photoId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `friend`
--

CREATE TABLE IF NOT EXISTS `friend` (
  `userId1` int(11) NOT NULL,
  `userId2` int(11) NOT NULL,
  `approved` enum('y','n') NOT NULL,
  PRIMARY KEY (`userId1`,`userId2`),
  KEY `userId2` (`userId2`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `photo`
--

CREATE TABLE IF NOT EXISTS `photo` (
  `photoId` int(11) NOT NULL AUTO_INCREMENT,
  `ownerId` int(11) NOT NULL,
  `photoAccess` varchar(1) NOT NULL DEFAULT 'o',
  `title` varchar(20) DEFAULT NULL,
  `fileName` varchar(100) NOT NULL,
  `caption` varchar(50) DEFAULT NULL,
  `dateAdded` datetime NOT NULL,
  `category` varchar(20) DEFAULT NULL,
  `tags` varchar(50) DEFAULT NULL,
  `location` varchar(20) DEFAULT NULL,
  `noRatings` int(3) DEFAULT '0',
  `cumulativeRating` int(4) DEFAULT '0',
  `rating` int(1) DEFAULT NULL,
  `lastRatingDate` datetime DEFAULT NULL,
  `lastRatingValue` int(1) DEFAULT NULL,
  `flagged` enum('y','n') DEFAULT 'n',
  `views` int(11) DEFAULT '0',
  `colour` varchar(1) DEFAULT NULL,
  `interestingness` int(11) DEFAULT '0',
  `camera` varchar(50) DEFAULT NULL,
  `fStop` varchar(6) DEFAULT NULL,
  `shutterSpeed` varchar(10) DEFAULT NULL,
  `dateTaken` datetime DEFAULT NULL,
  `isoSpeed` varchar(5) DEFAULT NULL,
  `focalLength` varchar(10) DEFAULT NULL,
  `flash` varchar(3) DEFAULT NULL,
  PRIMARY KEY (`photoId`),
  KEY `ownerId` (`ownerId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=112 ;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `userId` int(11) NOT NULL AUTO_INCREMENT,
  `userType` varchar(1) NOT NULL DEFAULT 'u',
  `username` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL,
  `avatar` varchar(20) NOT NULL DEFAULT 'avatar.jpg',
  `firstName` varchar(20) DEFAULT NULL,
  `lastName` varchar(20) DEFAULT NULL,
  `email` varchar(30) NOT NULL,
  `otherInfo` varchar(300) DEFAULT NULL,
  `dateJoined` datetime DEFAULT NULL,
  `lastLogin` datetime DEFAULT NULL,
  `offences` int(1) DEFAULT '0',
  `profileViews` int(11) DEFAULT '0',
  `profileComments` int(11) DEFAULT '0',
  `prolificacy` int(11) DEFAULT '0',
  PRIMARY KEY (`userId`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Constraints for table `album`
--
ALTER TABLE `album`
  ADD CONSTRAINT `album_ibfk_1` FOREIGN KEY (`ownerId`) REFERENCES `user` (`userId`);

--
-- Constraints for table `albumConn`
--
ALTER TABLE `albumConn`
  ADD CONSTRAINT `albumconn_ibfk_1` FOREIGN KEY (`photoId`) REFERENCES `photo` (`photoId`),
  ADD CONSTRAINT `albumconn_ibfk_2` FOREIGN KEY (`albumId`) REFERENCES `album` (`albumId`);

--
-- Constraints for table `comment`
--
ALTER TABLE `comment`
  ADD CONSTRAINT `comment_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `user` (`userId`),
  ADD CONSTRAINT `comment_ibfk_2` FOREIGN KEY (`photoId`) REFERENCES `photo` (`photoId`);

--
-- Constraints for table `favourite`
--
ALTER TABLE `favourite`
  ADD CONSTRAINT `favourite_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `user` (`userId`),
  ADD CONSTRAINT `favourite_ibfk_2` FOREIGN KEY (`photoId`) REFERENCES `photo` (`photoId`);

--
-- Constraints for table `friend`
--
ALTER TABLE `friend`
  ADD CONSTRAINT `friend_ibfk_1` FOREIGN KEY (`userId1`) REFERENCES `user` (`userId`),
  ADD CONSTRAINT `friend_ibfk_2` FOREIGN KEY (`userId2`) REFERENCES `user` (`userId`);

--
-- Constraints for table `photo`
--
ALTER TABLE `photo`
  ADD CONSTRAINT `photo_ibfk_1` FOREIGN KEY (`ownerId`) REFERENCES `user` (`userId`);
