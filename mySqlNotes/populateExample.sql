---------------------------//POPULATING TABLES\\---------------------------
-------------------------//POPULATING USER TABLE\\-------------------------
INSERT INTO user VALUES(
  NULL,
  'igneosaur',
  'dodgeviper',
  'Daniel',
  'Edwards',
  'igneosaur@googlemail.com',
  NULL,
  NOW(),
  NOW(),
  0,
  0,
  0,
  0
);

INSERT INTO user VALUES(
  NULL,
  'mattylee',
  'girls',
  'Matty',
  'Lee',
  'm.lee@yahoo.com',
  'I love breaking my bones.',
  NOW(),
  0,
  0,
  0,
  0
);

------------------------//POPULATING ALBUM TABLE\\-------------------------

INSERT INTO album VALUES(
  NULL,
  1,
  'Landscapes',
  NOW(),
  'Landscapes I Like',
  'Landscapes',
  'Landscapes, igneosaur',
  'Everywhere',
  0,
  0,
  NULL,
  0,
  0
);

------------------------//POPULATING PHOTO TABLE\\-------------------------

INSERT INTO photo VALUES(
  NULL,
  1,
  'Hong-Kong',
  'photos/1/IMG_2973.jpg',
  'Traffic',
  DATE_FORMAT(NOW(), '%D %M %Y at %l:%i%p'),
  'Night Shots',
  'Hong Kong, Night time, Motorway, Lights',
  'Hong Kong, China',
  1,
  0,
  0,
  NULL,
  'n',
  2,
  0,
  NULL,
  0
);

INSERT INTO photo VALUES(
  NULL,
  1,
  'Waterfall',
  'photos/1/IMG_3629.jpg',
  'Cascading',
  NOW(),
  'Scenery',
  'Scenery, Waterfalls',
  'El Salvador, Central America',
  1,
  0,
  0,
  NULL,
  'n',
  0,
  0,
  NULL,
  0
);

INSERT INTO photo VALUES(
  NULL,
  2,
  'Opera House',
  'photos/2/IMG_2892.jpg',
  'Roof',
  NOW(),
  'Travel',
  'Travel, Buildings, Sydney',
  'Sydney, Australia',
  NULL,
  0,
  0,
  NULL,
  'n',
  1,
  1,
  132790,
  0
);

------------------------//POPULATING FREIND TABLE\\------------------------

INSERT INTO friend VALUES(
  1,
  2
);

-----------------------//POPULATING COMMENT TABLE\\------------------------

INSERT INTO comment VALUES(
  1,
  3,
  'Pretty sweet Matty!',
  NOW(),
  'n'
);
