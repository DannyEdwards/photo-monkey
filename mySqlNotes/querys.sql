----------------------//QUERIES INVOLVED IN WEBSITE\\----------------------
--------------------------//UPDATING LASTLOGIN\\---------------------------
UPDATE photomonkey.user
SET lastLogin = NOW()
WHERE userId = '$userId';

-----------------------//SELECT INTERESTING IMAGES\\-----------------------
SELECT photoId, ownerId, fileName
FROM photomonkey.photo
ORDER BY interestingness DESC;

-------------------------//SELECT READABLE TIME\\--------------------------
SELECT *, DATE_FORMAT(dateAdded, '%D %M %Y at %l:%i%p')
FROM photomonkey.photo;

------------------------//count comments on photo\\-------------------------
SELECT COUNT(c.comment)
FROM user u JOIN comment c ON c.userId = u.userId
WHERE c.photoId = '$photoId'

------------------------//THE BEAST\\-------------------------
SELECT u.username, u.avatar, u.userId, c.comment, c.flagged, p.photoId, p.title, DATE_FORMAT(c.datePosted, '%D %M %Y at %l:%i%p')
FROM photo p JOIN comment c ON c.photoId = p.photoId
JOIN user u ON u.userId = c.userId
WHERE p.ownerId = 1
ORDER BY datePosted DESC
LIMIT 0,15