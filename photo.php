<?
/* 
* PHOTOMONKEY PHOTO PAGE
* PHOTO.PHP
*
* Author: Daniel Edwards w1056952
*/

session_start();
include 'dbconnect.php';

//these lines of code assign dollar variables to the session values for use in the code.
$sessUsername = $_SESSION['username'];
$sessUserId = $_SESSION['userId'];
$sessUserType = $_SESSION['userType'];

//creates a single variable to deduce whether any user is a logged in member or not.
if($sessUsername && $sessUserId && $sessUserType) $loggedIn = true;
else $loggedIn = false;

$photoId = $_GET['photoId'];

$page = $_GET['page'];
if(!$page) $page = 1;

//fetch and assign all relevant information for the photo.
$query =	"SELECT ownerId, photoAccess, title, fileName, caption, views, rating, noRatings, flagged, interestingness, camera, fStop, shutterSpeed, DATE_FORMAT(dateTaken, '%D %M %Y at %l:%i%p'), DATE_FORMAT(dateAdded, '%D %M %Y at %l:%i%p'), isoSpeed, focalLength, flash
			FROM photomonkey.photo
			WHERE photoId = '$photoId'";

querySecurity($query);

$result = mysql_query($query);

$record = mysql_fetch_assoc($result);

$ownerId = $record['ownerId'];
$photoAccess = $record['photoAccess'];
$title = $record['title'];
$fileName = $record['fileName'];
$caption = $record['caption'];
$views = $record['views'];
$rating = $record['rating'];
$noRatings = $record['noRatings'];
$flagged = $record['flagged'];
$interestingness = $record['interestingness'];
$dateAdded = $record["DATE_FORMAT(dateAdded, '%D %M %Y at %l:%i%p')"];

//kicks a non admin user to index.php if a photo is flagged.
if($flagged == 'y' && $sessUserType != 'a') header("Location: index.php");

//now the exif data
if ($record['camera']) {
	$exifData = true;
	$camera = $record['camera'];
	$fStop = $record['fStop'];
	$shutterSpeed = $record['shutterSpeed'];
	$dateTaken = $record["DATE_FORMAT(dateTaken, '%D %M %Y at %l:%i%p')"];
	$isoSpeed = $record['isoSpeed'];
	$focalLength = $record['focalLength'];
	$flash = $record['flash'];
}
else $exifData = false;

$fullResAvailable = protectPhoto($photoAccess);

if(!$title){
	$message = "Stop messing around mate...";
	$error = "This photo does not exist.";
	header("Location: error.php?message=$message&error=$error");
}

//fetch and assign all relevant information for the author.
$query = "SELECT username, avatar, DATE_FORMAT(dateJoined, '%D %M %Y') FROM photomonkey.user WHERE userId = '$ownerId'";
$result = mysql_query($query);

$record = mysql_fetch_assoc($result);
$ownerUsername = $record['username'];
$ownerAvatar = $record['avatar'];
$dateOwnerJoined = $record["DATE_FORMAT(dateJoined, '%D %M %Y')"];

//update dynamic aspects of the photo.
if(!isset($_GET['page'])){
	$views = $views+1;
	$interestingness = $interestingness+1;
	$query = "UPDATE photomonkey.photo SET views = '$views', interestingness = '$interestingness' WHERE photoId = '$photoId'";
	mysql_query($query);
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<!--head contains the .css file link, the favicon.ico link and the title-->
	<link rel="shortcut icon" href="images/favicon<? echo colourOfTheDay(); ?>.ico"/>
	<link rel="stylesheet" href="siteStyle.css"/>
	<script language="javascript" src="javascript.js"></script>
	<title>Photo</title>
</head>

<body>
<div id="wrapper">
	<!--HEADER-->
	<div id="logo" onClick="location.href='index.php'">
		<img src="images/logo<? echo colourOfTheDay(); ?>.png" alt="logo image" title="Logo" width="50px" /> <h1>photomonkey</h1>
	</div>

	<div id="search">
		<form action="search.php?area=tags" method="post" >
				<input type="text" name="searchBox" class="input" value="Search..." onClick="this.value=''"/>
				<input type="submit" value="Go" class="button" />
		</form>
	</div>
	
	<div id="topNav">
		<ul id="menu">
			<li><a href="index.php" id="tabLink">home</a></li>
			<li>
				<a href="explore.php?content=potd" id="tabLink" onmouseover="menuOpen('dropDownMenu')" onmouseout="menuClose()">let's go exploring...</a>
				<div id="dropDownMenu" onmouseover="cancelClose('dropDownMenu')" onmouseout="menuClose()">
					<a href="explore.php?content=bbint">by interestingness</a>
					<a href="explore.php?content=bbrec">by most resent</a>
			        <a href="explore.php?content=bbcat">by category</a>
			        <a href="explore.php?content=bbspec">by spectrum</a>
			        <a href="explore.php?content=bbcotd">by colour of the day</a>
					<a href="explore.php?content=rand">randomly</a>
	        	</div>
			</li>
			<li>
				<a href="profile.php?username=<? echo $sessUsername; ?>" id="tabLink" onMouseOver="menuOpen('dropDownMenu2')" onMouseOut="menuClose()" >your profile</a>
				<div id="dropDownMenu2" onMouseOver="cancelClose('dropDownMenu2')" onMouseOut="menuClose()">
					<a href="userPhotos.php?username=<? echo $sessUsername;?>">your photos</a>
			        <a href="editAccount.php?userId=<? echo $sessUserId; ?>">adjust account</a>
	        	</div>
			</li>
			<li><a href="upload.php" id="tabLink">upload</a></li>
			<li><a href="photo.php?photoId=<? echo $photoId; ?>&page=1" id="currentTabLink">photo</a></li>
		</ul>

		<ul id="menu" class="floatRight">
			<?
			if($loggedIn){
				
				echo '<li><a href="logout.php" id="tabLink">logout '.$sessUsername.'</a></li>';
				if($sessUserType == 'a') {
					echo '<li><a href="admin.php" id="tabLink">admin</a></li>';
				}
			}
			else{
				echo '<li><a href="login.php" id="tabLink">login / sign up</a></li>';
			}
			?>
		</ul>
	</div>

	<!--MAIN-->
	<div id="main">
		<h2><? echo $title; ?></h2><br />
		by <a href="profile.php?username=<? echo $ownerUsername; ?>"><? echo $ownerUsername; ?></a><br />
		<div id="full">
			<div id="photo">
				<?
				if($flagged == 'y'){
					echo "<div id=\"flaggedPhoto\">";
					echo "<i>Admin Notice: This image has been flagged for potential image violation.</i> <a href=\"admin.php\">Go to admin page</a>";
					echo "</div>";
				}
	
				echo "<img src=\"photos/$ownerId/scaled_$fileName\" alt=\"$ownerUsername's photo $title\" /><br />";
			   	if($caption) echo "<h3>$caption</h3><br />";
				?>
			</div>
			<br />
			<form name="ratingAndFaveForm" action="photoActions.php" method="post">
				<?
				if($rating){
					for ($i = 1; $i<6; $i++){
						if($i <= $rating) echo "<input type=\"image\" name=\"$i\" value=\"$i\" src=\"images/whitestar.png\" alt=\"white star image\" width=\"20\" /> ";
						else echo "<input type=\"image\" name=\"$i\" value=\"$i\" src=\"images/greystar.png\" alt=\"greyed out star image\" width=\"20\" /> ";
					}
					echo "<i><h3><b>$noRatings</b> Ratings</h3></i>";
				}
				else{
					for ($i = 1; $i<6; $i++){
						echo "<input type=\"image\" name=\"$i\" value=\"$i\" src=\"images/greystar.png\" alt=\"greyed out star image\" width=\"20\" /> ";
					}
					echo "<i><h3><b>$noRatings</b> Ratings</h3></i>";
				}
				?>
			
				<div id="floatRight">
					<div id="favouriteDiv"  onClick="submitRatingAndFaveForm()">
						<img src="images/favouriteHeart.png" alt="heart image" width="16" /><h3> Favourite</h3>
						<input type="hidden" name="favourite" />
					</div>
				</div>
				<input type="hidden" name="photoId" value="<? echo $photoId; ?>" />
			</form>
		</div>
		<div id="spacer"></div>
		<div id="large">
			<h2>Comments</h2>
			<div class="hr"></div>
			<?
			//this query counts records for paging.
			$countQuery =	"SELECT COUNT(*)
							FROM comment
							WHERE photoId = $photoId";
			$countResult = mysql_query($countQuery);
			$countRecord = mysql_fetch_assoc($countResult);
			$commentCount = $countRecord['COUNT(*)'];

			$commentsPerPage = 5;
			$noPages = ceil($commentCount/$commentsPerPage);//the ceil function rounds up.

			$query =	"SELECT u.userId, u.username, u.avatar, c.comment, c.datePosted, DATE_FORMAT(c.datePosted, '%D %M %Y at %l:%i%p'), c.flagged
						FROM user u JOIN comment c ON c.userId = u.userId
						WHERE c.photoId = $photoId
						ORDER BY datePosted DESC
						LIMIT ".(($page*$commentsPerPage)-($commentsPerPage)).",".($page*$commentsPerPage);
			$result = mysql_query($query);
            
			//this loop is used to uniquely identify comments.
			$formIdLoop = 1;

            while ($record = mysql_fetch_assoc($result)){
					echo "<div class=\"comment\">";
					echo "<div id=\"floatLeft\">";
                    echo "<img src=\"photos/".$record['userId']."/".$record['avatar']."\" width=\"30\" alt=\"".$record['username']."'s avatar\"/> ";
					echo "</div>";
                    echo "&nbsp<a href=\"profile.php?username=".$record['username']."\">".$record['username']."</a><br />";
                    echo "&nbsp<i>(".$record["DATE_FORMAT(c.datePosted, '%D %M %Y at %l:%i%p')"].")</i>";

					if($sessUsername && $sessUserId){
						//is the comment flagged or not?
						if($record['flagged'] == 'n'){
		                    echo "<div id=\"floatRight\">";
							echo "<form name=\"flagComment\" method=\"post\" id=\"flagCommentForm".$formIdLoop."\" action=\"photoActions.php\">";
							echo "<input type=\"hidden\" name=\"userId\" value=\"".$record['userId']."\" />";
							echo "<input type=\"hidden\" name=\"photoId\" value=\"".$photoId."\" />";
							echo "<input type=\"hidden\" name=\"datePosted\" value=\"".$record['datePosted']."\" />";
							echo "<input type=\"hidden\" name=\"flagComment\" value=\"y\" />";
							echo "<a href=\"javascript:submitForm('flagCommentForm".$formIdLoop."')\">Flag</a>";
							echo "</form>";
							echo "</div>";
						}
						else {
							echo "<div id=\"floatRight\">";
							echo "<form name=\"unflagComment\" method=\"post\" id=\"unflagCommentForm".$formIdLoop."\" action=\"photoActions.php\">";
							echo "<input type=\"hidden\" name=\"userId\" value=\"".$record['userId']."\" />";
							echo "<input type=\"hidden\" name=\"photoId\" value=\"".$photoId."\" />";
							echo "<input type=\"hidden\" name=\"datePosted\" value=\"".$record['datePosted']."\" />";
							echo "<input type=\"hidden\" name=\"unflagComment\" value=\"y\" />";
							echo "<a href=\"javascript:submitForm('unflagCommentForm".$formIdLoop."')\">Unflag</a>";
							echo "</form>";
							echo "</div>";
						}
					}

					echo "<br />";

                    if($record['flagged']=='n'){
						echo $record['comment']."<br />";
					}
                    else {
						
						echo "<i>This comment has been flagged, ";
						echo "<a href=\"\" onmouseover=\"showComment('flaggedComment".$formIdLoop."')\" onmouseout=\"hideComment('flaggedComment".$formIdLoop."')\" >Show</a></i><br />";
						echo "<span id=\"flaggedComment".$formIdLoop."\" style=\"visibility: hidden\">".$record['comment']."</span>";
					}
                    echo "<br />";
					//end of comment div.
					echo "</div>";
					$formIdLoop++;
            }

            if ($commentCount > 0) {
                //if there are comments present then the paging links are printed.
                if ($page > 1) echo "<a href=\"photo.php?photoId=$photoId&page=".($page-1)."\">< Newer Posts</a> | ";

                for ($i=1; $i<=$noPages; $i++){
                    if($i == $page && $i == $noPages) echo "Page $i";
                    else if($i == $page) echo "Page $i | ";
                    else echo "<a href=\"photo.php?photoId=$photoId&page=$i\">Page $i</a> | ";
                }

                if ($page < $noPages) echo "<a href=\"photo.php?photoId=$photoId&page=".($page+1)."\">Older Posts ></a>";

                echo "<br /><br />";
            }
            else {
                //if there are no comments found then this is printed.
                echo "<i>There are no comments here yet.</i><br /><br />";
            }
			
			echo "<h2>Leave a Comment</h2>";
			echo "<div class=\"hr\"></div>";

			if($loggedIn){
				//display comment form.
				?>
				<form name="commentForm" action="photoActions.php" method="post">
					<textarea name="comment" cols=50 rows=5 ></textarea><br />
					<input type="submit" name="sendComment" value="Comment" class="button" />
					<input type="reset" value="Clear" class="button" />
					<input type="hidden" name="photoId" value="<? echo $photoId; ?>" />
				</form>
				<?
			}
			else echo "<i>You need to be a logged in member to comment on a photo.</i>";
			?>
		</div>
		<div id="small">
			<h2>Photo Info</h2><div class="hr"></div>
			<?
			if ($exifData){
				echo "Camera: $camera<br />";
				echo "Shutter Speed $shutterSpeed<br />";
				echo "F Stop: $fStop<br />";
				echo "ISO Speed: $isoSpeed<br />";
				echo "Focal Length: $focalLength mm<br />";
				echo "Flash: $flash<br />";
				echo "Date Taken: $dateTaken<br /><br />";
			}
			else echo "<i>There is no technical data for this photo.</i><br /><br />";

			echo "Uploaded On: $dateAdded<br />";

			echo "Full Resolution Available:";
			if($fullResAvailable) {
				echo "<a href=\"photos/$ownerId/$fileName\" >Yes, Click Here</a>";
			}
			else echo "No";

			echo "<br />";

			if($loggedIn){
				//flag photo form.
				echo "<form name=\"flagPhoto\" id=\"flagPhotoForm\" method=\"post\" action=\"photoActions.php\">";
				echo "<input type=\"hidden\" name=\"photoId\" value=\"".$photoId."\" />";
				echo "<input type=\"hidden\" name=\"flagPhoto\" value=\"y\" />";
				echo "<a href=\"javascript:submitForm('flagPhotoForm')\">Flag</a> this photo for image violation.<br />";
				echo "</form>";
			}
			
			echo "<br />";
			echo "<h2>Photographer</h2><div class=\"hr\"></div>";

			if($sessUsername == $ownerUsername && $sessUserId == $ownerId){
				echo "I think you know who's photo this is don't you? You narcissist!<br />";
				echo "Since you're already here massaging your own ego, lets do something constructive shall we?<br /><br />";
				echo "<a href=\"editPhoto.php?fileName=$fileName\">Edit This Photo</a><br /><a href=\"removePhoto.php?photoId=$photoId\">Remove This Photo (God Forbid!)</a>";
			}
			else {
				echo "<img src=\"photos/$ownerId/$ownerAvatar\" width=\"50\" alt=\"$ownerUsername's avatar\"/><br />";
				echo "Username: <a href=\"profile.php?username=$ownerUsername\">$ownerUsername</a><br />";
				echo "Member Since: $dateOwnerJoined<br />";
				echo "Take a look at some of $ownerUsername's other photos:<br />";

				$query = "SELECT photoId, fileName, title FROM photomonkey.photo WHERE ownerId = $ownerId ORDER BY RAND();";
				$result = mysql_query($query);
				

				echo "<table>";
		
				for ($rows = 0; $rows<5; $rows++){
					echo "<tr>";
					for($columns = 0; $columns<5; $columns++){
	
						$record = mysql_fetch_assoc($result);
						$fileName = $record['fileName'];
						$photoId = $record['photoId'];
						$thumbTitle = $record['title'];
		
						if($record){
						echo "<td><a href=\"photo.php?photoId=$photoId\"><img src=\"photos/$ownerId/thumb_$fileName\" title=\"$thumbTitle\" alt=\"$ownerUsername's photo $thumbTitle\" width=\"53\" border=\"0\" /></a></td>";
						}
						else break;

					}
					echo "</tr>";
				}
	
				echo "</table>";

			}
			?>
		</div>
		<div id="clear"></div>
	</div>
	
	<!--FOOTER-->
	<ul id="menu">
		<li><a href="about.php" id="tabLink">about</a></li>
	</ul>
</div>
</body>

</html>