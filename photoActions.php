<?
/* 
* PHOTOMONKEY ACTIONS FOR PHOTOS
* PHOTOACTIONS.PHP
*
* Author: Daniel Edwards w1056952
*/

session_start();
include 'dbconnect.php';

if(!isset($_SESSION['userId']) && !isset($_SESSION['username'])){
		header("Location: login.php?redirection=restrictedContent");
}

$sessUsername = $_SESSION['username'];
$sessUserId = $_SESSION['userId'];
$photoId = $_POST['photoId'];

if (isset($_POST['sendComment']) && isset($_POST['comment'])){
	//this one posts a comment.

	//the comment is put through a function that escapes any illegal characters, so that punctiation may be used in the comment. 
	$comment = mysql_real_escape_string($_POST['comment']);

	$query =	"SELECT userId
				FROM photomonkey.user
				WHERE username = '$sessUsername'";
	$result = mysql_query($query);
	$record = mysql_fetch_assoc($result);
	$sessUserId = $record['userId'];

	$query =	"INSERT INTO photomonkey.comment (userId, photoId, comment, datePosted)
				VALUES ('$sessUserId', '$photoId', '$comment', NOW());";
	
	mysql_query($query) or die (mysql_error());

	header("Location: photo.php?photoId=".$photoId);

}

if (isset($_POST['favourite'])){
	//this one favourites a photo.
	$query = "INSERT INTO photomonkey.favourite VALUES ('$sessUserId', '$photoId', NOW());";
	
	mysql_query($query);

	header("Location: photo.php?photoId=".$photoId."&page=1");

}

if(isset($_POST['1']) || isset($_POST['2']) || isset($_POST['3']) || isset($_POST['4']) || isset($_POST['5'])){
	//this one rates a photo.
	if(isset($_POST['1'])) $rating = 1;
	if(isset($_POST['2'])) $rating = 2;
	if(isset($_POST['3'])) $rating = 3;
	if(isset($_POST['4'])) $rating = 4;
	if(isset($_POST['5'])) $rating = 5;
	$lastRating = $rating;

	$query =	"SELECT noRatings, cumulativeRating, interestingness
				FROM photomonkey.photo
				WHERE photoId = '$photoId'";
	$result = mysql_query($query);
	$record = mysql_fetch_assoc($result);
	$noRatings = $record['noRatings'];
	$cumulativeRating = $record['cumulativeRating'];
	$interestingness = $record['interestingness'];

	/*
	The ratings increase the interestingness of the photo by various magnitutes based on the rating.
	5 star rating adds a value of 3 to interestingness
	4 star rating adds a value of 2 to interestingness
	3 star rating adds a value of 1 to interestingness
	2 star rating does nothing to the interestingness.
	1 star rating takes a value of 1 from interestingness
	*/
	$interestingness = $interestingness+($rating-2);
	
	//the new rating is calculated.
	$cumulativeRating = $cumulativeRating+$rating;
	$noRatings = $noRatings+1;
	$rating = $cumulativeRating/$noRatings;

	$query =	"UPDATE photomonkey.photo
				SET noRatings = '$noRatings', cumulativeRating = '$cumulativeRating', rating = '$rating', lastRatingDate=NOW(), lastRatingValue='$lastRating', interestingness = '$interestingness'
				WHERE photoId = '$photoId'";
	mysql_query($query);

	header("Location: photo.php?photoId=".$photoId."&page=1");
}

if(isset($_POST['flagComment'])){
	$userId = $_POST['userId'];
	$photoId = $_POST['photoId'];
	$datePosted = $_POST['datePosted'];

	$query =	"UPDATE photomonkey.comment
				SET flagged = 'y'
				WHERE userId = '$userId' AND photoId='$photoId' AND datePosted = '$datePosted'";

	mysql_query($query);

	header("Location: photo.php?photoId=".$photoId."&page=1");
}

if(isset($_POST['unflagComment'])){
	$userId = $_POST['userId'];
	$photoId = $_POST['photoId'];
	$datePosted = $_POST['datePosted'];

	$query =	"UPDATE photomonkey.comment
				SET flagged = 'n'
				WHERE userId = '$userId' AND photoId='$photoId' AND datePosted = '$datePosted'";

	mysql_query($query);

	header("Location: photo.php?photoId=".$photoId."&page=1");
}

if(isset($_POST['flagPhoto'])){
	$photoId = $_POST['photoId'];

	$query =	"UPDATE photomonkey.photo
				SET flagged = 'y'
				WHERE photoId='$photoId'";

	mysql_query($query);

	header("Location: index.php");
}

?>