<?
/* 
* PHOTOMONKEY PROFILE PAGE
* PROFILE.PHP
*
* Author: Daniel Edwards w1056952
*/

session_start();
include 'dbconnect.php';

//these lines of code assign dollar variables to the session values for use in the code.
$sessUsername = $_SESSION['username'];
$sessUserId = $_SESSION['userId'];
$sessUserType = $_SESSION['userType'];

//creates a single variable to deduce whether any user is a logged in member or not.
if($sessUsername && $sessUserId && $sessUserType) $loggedIn = true;
else $loggedIn = false;

$urlUsername = $_GET['username'];

/*member can look at news relevent to them in three different areas:
- comments: can see all comments made on their photos.
- ratings: can see the last ratings made on any of their photos.
- friends: can see any friend activity that has happened.
*/
$newsArea = $_GET['newsArea'];
//default area is comments.
if(!$newsArea) $newsArea = "comments";

//gather and assign info on the username passed in the url.
$query =	"SELECT userId, avatar, firstName, lastName, otherInfo, DATE_FORMAT(dateJoined, '%D %M %Y at %l:%i%p'), DATE_FORMAT(lastLogin, '%D %M %Y at %l:%i%p') 
			FROM photomonkey.user
			WHERE username = '$urlUsername'";
$result = mysql_query($query);
$record = mysql_fetch_assoc($result);

if(!$record){
	$message = "Stop messing around mate...";
	$error = "This user does not exist.";
	header("Location: error.php?message=$message&error=$error");
}

$userId = $record['userId'];
$avatar = $record['avatar'];
$firstName = $record['firstName'];
$lastName = $record['lastName'];
$otherInfo = $record['otherInfo']; 
$dateJoined = $record["DATE_FORMAT(dateJoined, '%D %M %Y at %l:%i%p')"];
$lastLogin = $record["DATE_FORMAT(lastLogin, '%D %M %Y at %l:%i%p')"];

$query =	"SELECT camera
			FROM photomonkey.photo
			WHERE ownerId = '$userId'
			ORDER BY dateTaken DESC";
$result = mysql_query($query);
$record = mysql_fetch_assoc($result);

$camera = $record['camera'];

if(!$urlUsername){
	header("Location: login.php?redirection=restrictedContent");
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<!--head contains the .css file link, the favicon.ico link and the title-->
	<link rel="shortcut icon" href="images/favicon<? echo colourOfTheDay(); ?>.ico"/>
	<link rel="stylesheet" href="siteStyle.css"/>
	<script language="javascript" src="javascript.js"></script>
	<title><? echo $urlUsername; ?>'s Profile</title>
</head>

<body>
<div id="wrapper">
	<!--HEADER-->
	<div id="logo" onClick="location.href='index.php'">
		<img src="images/logo<? echo colourOfTheDay(); ?>.png" alt="logo image" title="Logo" width="50px" /> <h1>photomonkey</h1>
	</div>

	<div id="search">
		<form action="search.php?area=tags" method="post" >
				<input type="text" name="searchBox" class="input" value="Search..." onClick="this.value=''"/>
				<input type="submit" value="Go" class="button" />
		</form>
	</div>
	
	<div id="topNav">
		<ul id="menu">
			<li><a href="index.php" id="tabLink">home</a></li>
			<li>
				<a href="explore.php?content=potd" id="tabLink" onmouseover="menuOpen('dropDownMenu')" onmouseout="menuClose()">let's go exploring...</a>
				<div id="dropDownMenu" onmouseover="cancelClose('dropDownMenu')" onmouseout="menuClose()">
					<a href="explore.php?content=bbint">by interestingness</a>
					<a href="explore.php?content=bbrec">by most resent</a>
			        <a href="explore.php?content=bbcat">by category</a>
			        <a href="explore.php?content=bbspec">by spectrum</a>
			        <a href="explore.php?content=bbcotd">by colour of the day</a>
					<a href="explore.php?content=rand">randomly</a>
	        	</div>
			</li>
			<li>
				<a href="profile.php?username=<? echo $sessUsername; ?>" id="currentTabLink" onMouseOver="menuOpen('dropDownMenu2')" onMouseOut="menuClose()" >your profile</a>
				<div id="dropDownMenu2" onMouseOver="cancelClose('dropDownMenu2')" onMouseOut="menuClose()">
					<a href="userPhotos.php?username=<? echo $sessUsername;?>">your photos</a>
			        <a href="editAccount.php?userId=<? echo $sessUserId; ?>">adjust account</a>
	        	</div>
			</li>
			<li><a href="upload.php" id="tabLink">upload</a></li>
		</ul>

		<ul id="menu" class="floatRight">
			<?
			if($loggedIn){
				
				echo '<li><a href="logout.php" id="tabLink">logout '.$sessUsername.'</a></li>';
				if($sessUserType == 'a') {
					echo '<li><a href="admin.php" id="tabLink">admin</a></li>';
				}
			}
			else{
				echo '<li><a href="login.php" id="tabLink">login / sign up</a></li>';
			}
			?>
		</ul>
	</div>

	<!MAIN>
	<div id="main">
		<?
		if($sessUsername == $urlUsername && $sessUserId == $userId){
			//if the owner of the profile page is visiting.
			$commentQuery =	"SELECT u.username, u.userId, u.avatar, c.comment, c.flagged, p.photoId, p.title, DATE_FORMAT(c.datePosted, '%D %M %Y at %l:%i%p')
						FROM photo p JOIN comment c ON c.photoId = p.photoId
						JOIN user u ON u.userId = c.userId
						WHERE p.ownerId = '$sessUserId'
						ORDER BY datePosted DESC
						LIMIT 0,15";
			$commentResult = mysql_query($commentQuery);

			$ratingsQuery = "SELECT DATE_FORMAT(lastRatingDate, '%D %M %Y at %l:%i%p'), lastRatingValue, photoId, title, fileName
							FROM photo
							WHERE ownerId = $sessUserId
							AND lastRatingDate IS NOT NULL
							ORDER BY lastRatingDate DESC
							LIMIT 0,15";
			$ratingsResult = mysql_query($ratingsQuery);

			echo "<div id=\"floatLeft\"><img src=\"photos/$userId/$avatar\" alt=\"$urlUsername's avatar\" width=\"50\"/></div>";
			?>
			&nbsp <h2>Now then <? echo $sessUsername; ?>!</h2><br />
			&nbsp Welcome to your profile page.<br /><br />
			<div id="large">
				<h2>News</h2>
				<div id="floatRight"><br />
					<?
					if ($newsArea == "comments"){
						echo "comments";
					}
					else echo "<a href=\"profile.php?username=$urlUsername&newsArea=comments\">comments</a>";

					echo " | ";
 
					if ($newsArea == "ratings"){
						echo "ratings"; 
					}
					else echo "<a href=\"profile.php?username=$urlUsername&newsArea=ratings\">ratings</a>";

					?>
				</div>
				<div class="hr"></div>

				<?
				if($newsArea == "comments" || !$newsArea) {
					//lists the news regarding comments.
					if(!mysql_fetch_assoc($commentResult)){
						echo "<div class=\"comment\">";
							echo "<div id=\"floatLeft\"><img src=\"photos/1/avatar\" width=\"30\" /></div>";
							echo "&nbsp<a href=\"profile.php?username=\"Dan\">Dan</a>";
							echo " has a message for you ";
							echo "<a href=photo.php?photoId=".$commentRecord['photoId'].">".$commentRecord['title']."</a><br />";
							echo "<i> ($dateJoined)</i><br />";
							echo "Hello there newbie and welcome to photomonkey! Thanks for signing up. Feel free to wander around looking at all the wonderful photos that have been uploaded here and be sure to upload your own work too. Can't wait to see what you come up with.<br />Cheers,<br />Dan";
							echo "</div>";
					}
					else {
						$commentResult = mysql_query($commentQuery);
	
						while ($commentRecord = mysql_fetch_assoc($commentResult)){
							
							echo "<div class=\"comment\">";
							echo "<div id=\"floatLeft\"><img src=\"photos/".$commentRecord['userId']."/".$commentRecord['avatar']."\" width=\"30\" /></div>";
							echo "&nbsp<a href=\"profile.php?username=\"".$commentRecord['username']."\">".$commentRecord['username']."</a>";
							echo " commented on your photo ";
							echo "<a href=photo.php?photoId=".$commentRecord['photoId'].">".$commentRecord['title']."</a><br />";
							echo "<i> (".$commentRecord["DATE_FORMAT(c.datePosted, '%D %M %Y at %l:%i%p')"].")</i><br />";
	
							if($commentRecord['flagged']=='n') echo $commentRecord['comment']."<br />";
							else echo $commentRecord['comment']."<br /><i>This comment has been flagged by the community.</i><br />";
							echo "<br />";
							
							echo "</div>";
						}
					}
				}
				else if($newsArea == "ratings") {
					//lists the news regarding ratings.
					if(!mysql_fetch_assoc($ratingsResult)){
						echo "<i>There is no news in this area yet.</i>";
					}
					else {
						$ratingsResult = mysql_query($ratingsQuery);
	
						while ($ratingsRecord = mysql_fetch_assoc($ratingsResult)){
							
							echo "<div class=\"comment\">";
							echo "<div id=\"floatLeft\"><img src=\"photos/$sessUserId/thumb_".$ratingsRecord['fileName']."\" width=\"50\" alt=\"your rated photo\" /></div>";
							echo "&nbsp Your photo <a href=\"photo.php?photoId=".$ratingsRecord['photoId']."\">".$ratingsRecord['title']."</a> was rated<br />";
							echo "&nbsp<i>(".$ratingsRecord["DATE_FORMAT(lastRatingDate, '%D %M %Y at %l:%i%p')"].")</i><br />";
							
							for ($i = 1; $i<=$ratingsRecord['lastRatingValue']; $i++){
								echo "<img src=\"images/whiteStar.png\" width=\"20\" alt=\"white star image\" />";
							}

							echo "<br /><br />";
							echo "</div>";
						}
					}
				}
				?>
			</div>
			<div id="small">
				<h2>Your Photos</h2><div id="floatRight"><br /><a href="userPhotos.php?username=<? echo $sessUsername; ?>">See All</a></div>
				<div class="hr"></div>
				<?
				$query =	"SELECT photoId, fileName, title
							FROM photomonkey.photo
							WHERE ownerId = $sessUserId
							ORDER BY RAND()";
				$result = mysql_query($query);
				
				if(!mysql_fetch_assoc($result)){
					echo "<i>You haven't uploaded any photos yet. Why don't you go to the <a href=\"upload.php\">upload</a> section, and get some of your photos up here?</i>";
				}
				else{
					$result = mysql_query($query);

					echo "<table>";

					for ($rows = 0; $rows<5; $rows++){
						echo "<tr>";
						for($columns = 0; $columns<5; $columns++){

							$record = mysql_fetch_assoc($result);
							$fileName = $record['fileName'];
							$title = $record['title'];
							$photoId = $record['photoId'];

							if($record){
								echo "<td><a href=\"photo.php?photoId=$photoId\"><img src=\"photos/$sessUserId/thumb_$fileName\" alt=\"photo $fileName\" title=\"$title\" width=53 border=0 /></a></td>";
							}
							else break;

						}
						echo "</tr>";
					}

					echo "</table>";
				}
				?>
				<br /><br />
				<h2>Tools</h2>
				<div class="hr"></div>
				<a href="editAccount.php?userId=<? echo $sessUserId; ?>">Edit your account info</a><br />
				<a href="userPhotos.php?username=<? echo $sessUsername; ?>">View and edit all your photos</a><br />
				<br />
				<h2>Your Favourites</h2>
				<div class="hr"></div>
				<?
				$query =	"SELECT p.photoId, p.ownerId, p.fileName, p.title
							FROM photomonkey.photo p
							JOIN photomonkey.favourite f ON p.photoId = f.photoId
							WHERE userId = $sessUserId
							ORDER BY RAND( )";
				$result = mysql_query($query);
				
				if(!mysql_fetch_assoc($result)){
					echo "<i>You haven't got any favourites yet. Why don't you go <a href=\"explore.php\">exploring</a>?</i>";
				}
				else{
					$result = mysql_query($query);

					echo "<table>";

					for ($rows = 0; $rows<5; $rows++){
						echo "<tr>";
						for($columns = 0; $columns<5; $columns++){
							$record = mysql_fetch_assoc($result);
							$fileName = $record['fileName'];
							$title = $record['title'];
							$ownerId = $record['ownerId'];
							$photoId = $record['photoId'];
		
							if($record){
								echo "<td><a href=\"photo.php?photoId=$photoId\"><img src=\"photos/$ownerId/thumb_$fileName\" alt=\"photo $fileName\" title=\"$title\" width=53 border=0 /></a></td>";
							}
							else break;

						}
						echo "</tr>";
					}

					echo "</table>";
				}
				?>
				<br /><br />
				<h2>Friends</h2>
				<div class="hr"></div>
			</div>
			<?
		}
		else{
			//if a visitor is viewing a users page.
			$query =	"SELECT photoId, fileName, title
						FROM photomonkey.photo
						WHERE flagged = 'n' AND ownerId = '$userId'
						ORDER BY RAND();";
			$result = mysql_query($query);

			echo "<div id=\"floatLeft\"><img src=\"photos/$userId/$avatar\" alt=\"$urlUsername's avatar\" width=\"50\"/></div>";
			echo "&nbsp <h2>Welcome to $urlUsername's profile.</h2><br />";
			echo "&nbsp Here you can browse $urlUsername's content.<br /><br />";

			echo "<div id=\"large\">";
			echo "<h2>Some Photos</h2>";
			echo "<div id=\"floatRight\"><br />";
			echo "<a href=\"userPhotos.php?username=$urlUsername\" >See all of $urlUsername's photos</a></div>";
			echo "<div class=\"hr\"></div>";
			echo "<table>";

			if(!mysql_fetch_assoc($result)) echo "<i>Looks like $urlUsername hasn't uploaded any photos yet...</i>";
			
			$result = mysql_query($query);
			
			for ($rows = 0; $rows<5; $rows++){
				echo "<tr>";
				for($columns = 0; $columns<5; $columns++){

					$record = mysql_fetch_assoc($result);
					$fileName = $record['fileName'];
					$title = $record['title'];
					$photoId = $record['photoId'];
					
					if($record){
						echo "<td><a href=\"photo.php?photoId=$photoId\"><img src=\"photos/$userId/thumb_$fileName\" alt=\"photo $fileName\" title=\"$title\" width=93 border=0 /></a></td>";
					}
					else break;

				}
				echo "</tr>";
			}

			echo "</table>";
			echo "<a href=\"profile.php?username=$urlUsername\">Randomise!</a>";
			echo "</div>";

			echo "<div id=\"small\">";
			echo "<h2>User Info</h2><br /><div class=\"hr\"></div>";
			echo "Full Name: $firstName $lastName<br /><br />";
			echo "Member Since: $dateJoined<br /><br />";
			if($otherInfo) echo "About Me:<br />$otherInfo<br /><br />";
			if($camera) echo "Current Camera: $camera<br /><br />";
			echo "Last Logged In: $lastLogin<br /><br />";
			if($loggedIn){
				echo "<a href>Make friends with dan</a>";
			}
			echo "</div>";

		}
		?>
		<div id="clear"></div>
	</div>

	<!--FOOTER-->
	<ul id="menu">
		<li><a href="about.php" id="tabLink">about</a></li>
	</ul>
</div>
</body>

</html>