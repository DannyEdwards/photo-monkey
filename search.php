<?
/* 
* PHOTOMONKEY SEARCH PAGE
* SEARCH.PHP
*
* Author: Daniel Edwards w1056952
*/

include 'dbconnect.php';

//these lines of code assign dollar variables to the session values for use in the code.
$sessUsername = $_SESSION['username'];
$sessUserId = $_SESSION['userId'];
$sessUserType = $_SESSION['userType'];

//creates a single variable to deduce whether any user is a logged in member or not.
if($sessUsername && $sessUserId && $sessUserType) $loggedIn = true;
else $loggedIn = false;

$searchArea = $_GET['area'];
$term = $_POST['searchBox'];
if(!$term) $term = $_GET['term'];
/*
Search areas can be:
	Photo Tags (photo table)
	Members (users table)
*/

if($searchArea == "tags"){
	$tagsQuery = "SELECT photoId, ownerId, fileName, tags, title FROM photomonkey.photo WHERE tags LIKE '%$term%'";
	$tagsResult = mysql_query($tagsQuery);
}
if($searchArea == "members"){
	$membersQuery = "SELECT userId, username, avatar, firstName, lastName, DATE_FORMAT(dateJoined, '%D %M %Y') FROM photomonkey.user WHERE username LIKE '%$term%'";
	$membersResult = mysql_query($membersQuery);
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<!--head contains the .css file link, the favicon.ico link and the title-->
	<link rel="shortcut icon" href="images/favicon<? echo colourOfTheDay(); ?>.ico"/>
	<link rel="stylesheet" href="siteStyle.css"/>
	<script language="javascript" src="javascript.js"></script>
	<title>Search</title>
</head>

<body>
<div id="wrapper">
	<!--HEADER-->
	<div id="logo" onClick="location.href='index.php'">
		<img src="images/logo<? echo colourOfTheDay(); ?>.png" alt="logo image" title="Logo" width="50px" /> <h1>photomonkey</h1>
	</div>

	<div id="search">
		<form action="search.php?area=tags" method="post" >
				<input type="text" name="searchBox" class="input" value="Search..." onClick="this.value=''"/>
				<input type="submit" value="Go" class="button" />
		</form>
	</div>
	
	<div id="topNav">
		<ul id="menu">
			<li><a href="index.php" id="tabLink">home</a></li>
			<li>
				<a href="explore.php?content=potd" id="tabLink" onmouseover="menuOpen('dropDownMenu')" onmouseout="menuClose()">let's go exploring...</a>
				<div id="dropDownMenu" onmouseover="cancelClose('dropDownMenu')" onmouseout="menuClose()">
					<a href="explore.php?content=bbint">by interestingness</a>
					<a href="explore.php?content=bbrec">by most resent</a>
			        <a href="explore.php?content=bbcat">by category</a>
			        <a href="explore.php?content=bbspec">by spectrum</a>
			        <a href="explore.php?content=bbcotd">by colour of the day</a>
					<a href="explore.php?content=rand">randomly</a>
	        	</div>
			</li>
			<li>
				<a href="profile.php?username=<? echo $sessUsername; ?>" id="tabLink" onMouseOver="menuOpen('dropDownMenu2')" onMouseOut="menuClose()" >your profile</a>
				<div id="dropDownMenu2" onMouseOver="cancelClose('dropDownMenu2')" onMouseOut="menuClose()">
					<a href="userPhotos.php?username=<? echo $sessUsername;?>">your photos</a>
			        <a href="editAccount.php?userId=<? echo $sessUserId; ?>">adjust account</a>
	        	</div>
			</li>
			<li><a href="upload.php" id="tabLink">upload</a></li>
		</ul>

		<ul id="menu" class="floatRight">
			<?
			if($loggedIn){
				
				echo '<li><a href="logout.php" id="tabLink">logout '.$sessUsername.'</a></li>';
				if($sessUserType == 'a') {
					echo '<li><a href="admin.php" id="tabLink">admin</a></li>';
				}
			}
			else{
				echo '<li><a href="login.php" id="tabLink">login / sign up</a></li>';
			}
			?>
		</ul>
	</div>
	
	<!--MAIN-->
	<div id="main">
		<h2>Search Results</h2><br />
		<div id="full">
			<?
			if($searchArea == "tags") {
				echo "<h2>Searching Tags for \"$term\"</h2>";
				echo "<div id=\"floatRight\"><br />";
				echo "tags | <a href=\"search.php?area=members&term=$term\">members</a>";
				echo "</div>";
				echo "<br /><div class=\"hr\"></div>";

				if(!mysql_fetch_assoc($tagsResult)) echo "<i>Your search returned with no results.</i><br />";
				else {
					$tagsResult = mysql_query($tagsQuery);
					
					while ($record = mysql_fetch_assoc($tagsResult)){
						$photoId = $record['photoId'];
						$ownerId = $record['ownerId'];
						$fileName = $record['fileName'];
						$tags = $record['tags'];
						$title = $record['title'];
		
						echo "<div id=\"floatLeft\">";
						echo "<a href=\"photo.php?photoId=$photoId\"><img src=\"photos/$ownerId/thumb_$fileName\" alt=\"a photo matching your query\" width=100 border=0 /></a>";
						echo "</div>";
						echo "&nbsp Title: <a href=photo.php?photoId=$photoId />$title</a><br />";
						echo "&nbsp Tags: $tags<br /><br /><br /><br /><br /><br />";
					}
				}
			}
			else if ($searchArea == "members") {
				echo "<h2>Searching Members for \"$term\"</h2>";
				echo "<div id=\"floatRight\"><br />";
				echo "<a href=\"search.php?area=tags&term=$term\">tags</a> | members";
				echo "</div>";
				echo "<br /><div class=\"hr\"></div>";

				if(!mysql_fetch_assoc($membersResult)) echo "<i>Your search returned with no results.</i><br />";
				else {
					$membersResult = mysql_query($membersQuery);
					
					while ($record = mysql_fetch_assoc($membersResult)){
						$userId = $record['userId'];
						$username = $record['username'];
						$avatar = $record['avatar'];
						$firstName = $record['firstName'];
						$lastName = $record['lastName'];
						$dateJoined = $record["DATE_FORMAT(dateJoined, '%D %M %Y')"];

						echo "<div id=\"floatLeft\"><a href=\"profile.php?username=$username\"><img src=\"photos/$userId/$avatar\" alt=\"$username's avatar\" width=\"110\" border=\"0\" /></a></div>";
						echo "&nbsp Username: <a href=\"profile.php?username=$username\">$username</a><br />";
						echo "&nbsp Name: $firstName $lastName<br />";
						echo "&nbsp Member Since: $dateJoined<br /><br /><br /><br /><br /><br />";;
					}
				}
			}
		?>
		<div id="clear"></div>
		</div>
	
	<!--end of main div-->
	</div>
	
	<!--FOOTER-->
	<ul id="menu">
		<li><a href="about.php" id="tabLink">about</a></li>
	</ul>
</div>
</body>

</html>