<?
/* 
* PHOTOMONKEY SIGN UP ACTIONS
* SIGNUP.PHP
*
* Author: Daniel Edwards w1056952
*/

session_start();
include 'dbconnect.php';

if (isset($_POST['send'])){
	$username = $_POST['signUpUsername'];
	$password = crypt($_POST['password1'], "pw");
	$email = $_POST['email'];
	$firstName = $_POST['firstName'];
	$lastName = $_POST['lastName'];
	$comments = $_POST['comments'];
	$avatar = $_FILES["avatar"];

	if(!isset($username) || $username == "" || !isset($password) || $password == "" || !isset($email) || $email == "") header("Location: login.php?&redirection=SignUpFail");

	$query = "INSERT INTO photomonkey.user (username, password, firstName, lastName, email, otherInfo, dateJoined, lastLogin) VALUES ('$username', '$password', '$firstName', '$lastName', '$email', '$comments', NOW(), NOW());";
	
	
	mysql_query($query);

	$result = mysql_query("SELECT userId, userType FROM photomonkey.user WHERE username = '$username'");
	$record = mysql_fetch_assoc($result);
	
	$userId = $record['userId'];
	$userType = $record['userType'];
	
	mkdir("photos/$userId/");

	if($_FILES["avatar"]["error"] == 4) {
		//if there is no avatar sent, we assign the default blank avatar.
		copy("images/avatar.jpg", "photos/$userId/avatar.jpg");
	}
	else {
		//if an avatar has been submitted in the previous form then we can begin to deal with the file wich will result in a scaled down thumbnail of the original image.
		//max file size in bytes.
		$maxFileSize = 5*1024*1024;
	
		if ($_FILES["avatar"]["type"] == "image/jpeg") $extension = ".jpg";
		if ($_FILES["avatar"]["type"] == "image/gif") $extension = ".gif";
		if ($_FILES["avatar"]["type"] == "image/pjpeg") $extension = ".png";
		
		//these if statements determine if the files being uploaded are of a valid file type and size.
		//the result is stored into a variable to be used next.
		if ($extension && ($_FILES["avatar"]["size"] < $maxFileSize)) $fileTypeValid = true; 
		else $fileTypeValid = false;
	
		//if the file type is valid we continue with the upload.
		//error handling standards from w3schools.com.
		if ($fileTypeValid){
			if ($_FILES["avatar"]["error"] > 0){
				$error = $_FILES["avatar"]["error"];
				$message = "Somethings gone wrong with your upload. Give it another go.";
				header("Location: error.php?message=$message&error=$error");
			}
			else{
				//create the new file name called "avatar" plus extension.
				$fileName = "avatar".$extension;

				if (file_exists("photos/$userId/".$fileName)){
					//this should actually never occur.
					$error = "There was a problem with uploading your avatar, in fact, this should never happen!";
					$message = "Something Went Wrong.";
					header("Location: error.php?error=$error&message=$message");
 				}
    			else {
					//everything seems fine so now we start the image manipulation.
					//The following code for image manipulation is a portion from upload.php.

					//get image sizes and type information.
					$size = getimagesize($_FILES['avatar']['tmp_name']);
					//$size[0] is width.
					//$size[1] is height.
					//$size[2] is type.
				
					//load image into $image variable. supports .jpeg, .gif and .png.
					if($extension == ".jpg") {
						$image = imagecreatefromjpeg($_FILES['avatar']['tmp_name']);
					}
					elseif($extension == ".gif") {
						$image = imagecreatefromgif($_FILES['avatar']['tmp_name']);
					}
					elseif($extension == ".png") {
						$image = imagecreatefrompng($_FILES['avatar']['tmp_name']);
					}

					if($size[0] > $size[1]) {
						//landscape.
						$biggestSide = $size[0]; //find biggest length to be used later.
					}
					else {
						//portrait.
						$biggestSide = $size[1]; //find biggest length to be used later.
					}

					//now we create the square thumbnail image form the original image desegnated to be the avatar.
					//The crop size will be a third that of the largest side 
					$cropPercent = 0.66; // This will zoom in to 66% zoom (crop)
					$cropWidth   = $biggestSide*$cropPercent; 
					$cropHeight  = $biggestSide*$cropPercent;            
			
					//getting the top left coordinate
					$x = ($size[0]-$cropWidth)/2;
					$y = ($size[1]-$cropHeight)/2;
				
					$avatarSize = 100; // will create a 100 x 100 thumb
					$avatarImage = imagecreatetruecolor($avatarSize, $avatarSize); 
					
					imagecopyresampled($avatarImage, $image, 0, 0,$x, $y, $avatarSize, $avatarSize, $cropWidth, $cropHeight);
					
					$jpegCompression = 100;
					$permissions=null;

					//save square thumb photo.
					if($extension == ".jpg") {
						imagejpeg($avatarImage,"photos/".$userId."/".$fileName, $jpegCompression);
					}
					elseif($extension == ".gif") {
						imagegif($avatarImage,"photos/".$userId."/".$fileName);         
					}
					elseif($extension == ".png") {
						imagepng($avatarImage,"photos/".$userId."/".$fileName);
					}
   
					if($permissions != null) {
						chmod($_FILES['avatar']['tmp_name'],$permissions);
					}

					//now add in the avatar info into the database.
					$query = "UPDATE photomonkey.user SET avatar = '$fileName' WHERE userId = '$userId'";
					mysql_query($query);
				}

			}
		}
		else{
			$message = "Invalid file type.";
			$error = "Avatar image must be .jpeg, .gif or a .png.";
			header("Location: error.php?message=$message&error=$error");
		}
	}
	
	//the session is adapted using the users unique identifyers.
	$_SESSION['userId']= $userId;  
	$_SESSION['username'] = $username;
	$_SESSION['userType'] = $userType;
	
	//the users lastLogin in attribute is updated to the current time.
	$query =	"UPDATE photomonkey.user
				SET lastLogin = NOW()
				WHERE userId = '$userId'";
	mysql_query($query);

	header("Location: profile.php?username=$username"); 
}

?>