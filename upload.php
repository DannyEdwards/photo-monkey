<?
/* 
* PHOTOMONKEY UPLOAD PAGE
* UPLOAD.PHP
*
* Author: Daniel Edwards w1056952
*/

session_start();
include 'dbconnect.php';
protectPage();

//these lines of code assign dollar variables to the session values for use in the code.
$sessUsername = $_SESSION['username'];
$sessUserId = $_SESSION['userId'];
$sessUserType = $_SESSION['userType'];

//creates a single variable to deduce whether any user is a logged in member or not.
if($sessUsername && $sessUserId && $sessUserType) $loggedIn = true;
else $loggedIn = false;

//max file size in bytes.
$maxFileSize = 5*1024*1024;

if (isset($_POST['submit'])){

	$userId = $_SESSION['userId'];
	
	//this whole script is looped through for each file that has been used.
	for($i=1; $i<=5; $i++){
		//checks whether file is present.
		if($_FILES["file$i"]["error"] != 4) {
	
			if ($_FILES["file$i"]["type"] == "image/jpeg") $extension = ".jpg";
			if ($_FILES["file$i"]["type"] == "image/gif") $extension = ".gif";
			if ($_FILES["file$i"]["type"] == "image/pjpeg") $extension = ".png";
		
			//these if statements determine if the files being uploaded are of a valid file type and size.
			//the result is stored into a variable to be used next.
			if ($extension && ($_FILES["file$i"]["size"] < $maxFileSize)) $fileTypeValid = true;
			else $fileTypeValid = false;
			
			//if the file type is valid we continue with the upload.
			//error handling standards from w3schools.com.
			if ($fileTypeValid){
				if ($_FILES["file$i"]["error"] > 0){
					$error = $_FILES["file$i"]["error"];
					$message = "Somethings gone wrong with your upload. Give it another go.";
					header("Location: error.php?message=$message&error=$error");
				}
				else{
					//create a new random 8bit hex number for the file name.
					$hexFileName = substr(md5(rand()), 0, 8).$extension;
					//collect all relevent file info.
					$title = $_FILES["file$i"]["name"];
					$fileSize = ($_FILES["file$i"]["size"] / 1024);
					$tempFile = $_FILES["file$i"]["tmp_name"];
		
					//collecting exif data (if present).
					$exifData = exif_read_data($tempFile);
					if($exifData['Model']){
						$camera = $exifData['Model'];
						$fStop = $exifData['COMPUTED']['ApertureFNumber'];
						$shutterSpeed = $exifData['ExposureTime'];
						$dateTaken = $exifData['DateTimeOriginal'];
						$isoSpeed = $exifData['ISOSpeedRatings'];
		
						//deal with the focal length. The focal length in the exif data is given as a redundant fraction (i.e. 5500/1000 or 24/1), the next two limes of code split the fraction and converts it to the readable focal length in milimeters (i.e. 5.5 or 24mm).
						$splitFocalLength = (explode("/", $exifData['FocalLength']));
						$focalLength = $splitFocalLength[0]/$splitFocalLength[1];
		
						if($exifData['Flash']==16) $flash = "Off";
						else $flash = "On";
					}	
		
					if (file_exists("photos/$userId/" . $hexFileName)){
						$message = "Woah.";
						$error = "Something very improbable just happened, try uploading again. And go out and buy a lottery ticket!";
						header("Location: error.php?message=$message&error=$error");
						//this section would essentially never incur with the use of a random 8bit hex number. "Something very improbable has happened!"
		 			}
		    		else{
						//everything seems fine so now we start the image manipulation.
						/*This code for image manipulation is a heavy adaptation and combination of two free scripts downloaded from the internet:
						* 1.	SimpleImage.php
						*		Author: Simon Jarvis
						*		Link: http://www.white-hat-web-design.co.uk/articles/php-image-resizing.php
						* 2.	CropImage.php
						*		Author: Rendair (Forum Post)
						*		Link: http://www.talkphp.com/advanced-php-programming/1709-cropping-images-using-php.html
						*/
						
						//get image sizes and type information.
						$size = getimagesize($_FILES["file$i"]['tmp_name']);
						//$size[0] is width.
						//$size[1] is height.
						//$size[2] is type.
						
						//load image into $image variable. supports .jpeg, .gif and .png.
						if($extension == ".jpg") {
							$image = imagecreatefromjpeg($_FILES["file$i"]['tmp_name']);
						}
						elseif($extension == ".gif") {
							$image = imagecreatefromgif($_FILES["file$i"]['tmp_name']);
						}
						elseif($extension == ".png") {
							$image = imagecreatefrompng($_FILES["file$i"]['tmp_name']);
						}
		
						//save full resolution photo back to original file type.
						$jpegCompression = 100;
						$permissions=null;
						
						if($extension == ".jpg") {
							imagejpeg($image,"photos/" . $userId . "/".$hexFileName,$jpegCompression);
						}
						elseif($extension == ".gif") {
							imagegif($image,"photos/" . $userId . "/".$hexFileName);         
						}
						elseif($extension == ".png") {
							imagepng($image,"photos/" . $userId . "/".$hexFileName);
						}
		
						if( $permissions != null) {
							chmod($_FILES["file$i"]['tmp_name'],$permissions);
						}
		
						if($size[0] > $size[1]) {
							//landscape.
							$biggestSide = $size[0]; //find biggest length to be used later (in thumb).
							
							$width = 800;//this is the width to scale to if landscape.
							
							//get ratios right for adjacent side.
							$ratio = $width / $size[0];
							$height = $size[1] * $ratio;
							//then resize.
							$newImage = imagecreatetruecolor($width, $height);//this creates a blank image of said sizes.
							imagecopyresampled($newImage, $image, 0, 0, 0, 0, $width, $height, $size[0], $size[1]);
							$scaledImage = $newImage; 
						}
						else {
							//portrait.
							$biggestSide = $size[1]; //find biggest length to be used later (in thumb).
							
							$height = 800;//this is the height to scale to if portrait.
							
							$ratio = $height / $size[1];
							$width = $size[0] * $ratio;
							//and resize.
							$new_image = imagecreatetruecolor($width, $height);//this creates a blank image of said sizes.
							imagecopyresampled($new_image, $image, 0, 0, 0, 0, $width, $height, $size[0], $size[1]);
							$scaledImage = $new_image;
						}
						
						//save scaled down photo back to original file type.
						if($extension == ".jpg") {
							imagejpeg($scaledImage,"photos/" . $userId . "/scaled_".$hexFileName,$jpegCompression);
						}
						elseif($extension == ".gif") {
							imagegif($scaledImage,"photos/" . $userId . "/scaled_".$hexFileName);         
						}
						elseif($extension == ".png") {
							imagepng($scaledImage,"photos/" . $userId . "/scaled_".$hexFileName);
						}
		
						if( $permissions != null) {
							chmod($_FILES["file$i"]['tmp_name'],$permissions);
						}
						
						//now we create the square thumbnail image.
						//The crop size will be a third that of the largest side 
						$cropPercent = 0.66; // This will zoom in to 66% zoom (crop)
						$cropWidth   = $biggestSide*$cropPercent; 
						$cropHeight  = $biggestSide*$cropPercent;            
					
						//getting the top left coordinate
						$x = ($size[0]-$cropWidth)/2;
						$y = ($size[1]-$cropHeight)/2;
						
						$thumbSize = 250; // will create a 250 x 250 thumb
						$thumbImage = imagecreatetruecolor($thumbSize, $thumbSize); 
							
						imagecopyresampled($thumbImage, $image, 0, 0,$x, $y, $thumbSize, $thumbSize, $cropWidth, $cropHeight);
							
						//save square thumb photo.
						if($extension == ".jpg") {
							imagejpeg($thumbImage,"photos/" . $userId . "/thumb_".$hexFileName,$jpegCompression);
						}
						elseif($extension == ".gif") {
							imagegif($thumbImage,"photos/" . $userId . "/thumb_".$hexFileName);         
						}
						elseif($extension == ".png") {
							imagepng($thumbImage,"photos/" . $userId . "/thumb_".$hexFileName);
						}
		   
						if($permissions != null) {
							chmod($_FILES["file$i"]['tmp_name'],$permissions);
						}
		      
						//now we insert the new photos data into the database.
						if($exifData['Model']){
							//also entered exifData if it is present in the file.
		    				$query = "INSERT INTO photomonkey.photo (ownerId, title, fileName, dateAdded, camera, fStop, shutterSpeed, dateTaken, isoSpeed, focalLength, flash) VALUES ('$userId', '$title', '$hexFileName', NOW(), '$camera', '$fStop', '$shutterSpeed', '$dateTaken', '$isoSpeed', '$focalLength', '$flash');";
						}
						else{
							$query = "INSERT INTO photomonkey.photo (ownerId, title, fileName, dateAdded) VALUES ('$userId', '$title', '$hexFileName', NOW());";
						}
		
						mysql_query($query);
						
		    			header("Location: userPhotos.php?username=$sessUsername&from=upload");
		    		}
				}
			}
			else{
				$message = "Invalid file type.";
				$error = "File must be .jpeg, .gif or a .png.";
				header("Location: error.php?message=$message&error=$error");
			}
		}
	}
	header("Location: userPhotos.php?username=$sessUsername&from=upload");
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<!--head contains the .css file link, the favicon.ico link and the title-->
	<link rel="shortcut icon" href="images/favicon<? echo colourOfTheDay(); ?>.ico"/>
	<link rel="stylesheet" href="siteStyle.css"/>
	<script language="javascript" src="javascript.js"></script>
	<title>Upload</title>
</head>

<body>
<div id="wrapper">
	<!--HEADER-->
	<div id="logo" onClick="location.href='index.php'">
		<img src="images/logo<? echo colourOfTheDay(); ?>.png" alt="logo image" title="Logo" width="50px" /> <h1>photomonkey</h1>
	</div>

	<div id="search">
		<form action="search.php?area=tags" method="post" >
			<input type="text" name="searchBox" class="input" value="Search..." onClick="this.value=''"/>
			<input type="submit" value="Go" class="button" />
		</form>
	</div>

	<div id="topNav">
		<ul id="menu">
			<li><a href="index.php" id="tabLink">home</a></li>
			<li>
				<a href="explore.php?content=potd" id="tabLink" onMouseOver="menuOpen('dropDownMenu1')" onMouseOut="menuClose()" >let's go exploring...</a>
				<div id="dropDownMenu1" onMouseOver="cancelClose('dropDownMenu1')" onMouseOut="menuClose()">
					<a href="explore.php?content=bbint">by interestingness</a>
					<a href="explore.php?content=bbrec">by most resent</a>
			        <a href="explore.php?content=bbcat">by category</a>
			        <a href="explore.php?content=bbspec">by spectrum</a>
			        <a href="explore.php?content=bbcotd">by colour of the day</a>
					<a href="explore.php?content=rand">randomly</a>
	        	</div>
			</li>
			<li>
				<a href="profile.php?username=<? echo $sessUsername; ?>" id="tabLink" onMouseOver="menuOpen('dropDownMenu2')" onMouseOut="menuClose()" >your profile</a>
				<div id="dropDownMenu2" onMouseOver="cancelClose('dropDownMenu2')" onMouseOut="menuClose()">
					<a href="userPhotos.php?username=<? echo $sessUsername;?>">your photos</a>
			        <a href="editAccount.php?userId=<? echo $sessUserId; ?>">adjust account</a>
	        	</div>
			</li>
			<li><a href="upload.php" id="currentTabLink">upload</a></li>
		</ul>

		<ul id="menu" class="floatRight">
			<?
			if($loggedIn){
				
				echo '<li><a href="logout.php" id="tabLink">logout '.$sessUsername.'</a></li>';
				if($sessUserType == 'a') {
					echo '<li><a href="admin.php" id="tabLink">admin</a></li>';
				}
			}
			else{
				echo '<li><a href="login.php" id="tabLink">login / sign up</a></li>';
			}
			?>
		</ul>
	</div>
	
	<!--MAIN-->
	<div id="main">
		<h2>Upload</h2><br />
		Upload your pictures here.<br>
		<div id="large">
			<form action="upload.php" method="post" enctype="multipart/form-data">
				<input type="file" name="file1" id="file" /><span id="file1Feedback"></span><br />
				<input type="file" name="file2" id="file" /><span id="file2Feedback"></span><br />
				<input type="file" name="file3" id="file" /><span id="file3Feedback"></span><br />
				<input type="file" name="file4" id="file" /><span id="file4Feedback"></span><br />
				<input type="file" name="file5" id="file" /><span id="file5Feedback"></span><br />
				<br />
				<input type="submit" name="submit" value="Submit" />
			</form>
		</div>
		<div id="small">
			<h2>Uploading</h2>
			<div class="hr"></div>
			Before you go mental uploading everything just make sure the file format is either a .jpg/.jpeg, .png or a .gif. Of course you can upload your full res images but just check that they are under <? echo $maxFileSize/1024/1024; ?>MB, I think that's resonable.
		</div>
	</div>
	
	<!--FOOTER-->
	<ul id="menu">
		<li><a href="about.php" id="tabLink">about</a></li>
	</ul>
</div>
</body>

</html>