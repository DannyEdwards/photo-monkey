<?
/* 
* PHOTOMONKEY USER PHOTOS PAGE
* USERPHOTOS.PHP
*
* Author: Daniel Edwards w1056952
*/

session_start();
include 'dbconnect.php';

$urlUsername = $_GET['username'];

//these lines of code assign dollar variables to the session values for use in the code.
$sessUsername = $_SESSION['username'];
$sessUserId = $_SESSION['userId'];
$sessUserType = $_SESSION['userType'];

//creates a single variable to deduce whether any user is a logged in member or not.
if($sessUsername && $sessUserId && $sessUserType) $loggedIn = true;
else $loggedIn = false;

$page = $_GET['page'];
if(!$page) $page = 1;

//gather and assign info on the username passed in the url.
$query =	"SELECT userId, avatar 
			FROM photomonkey.user
			WHERE username = '$urlUsername'";
$result = mysql_query($query);
$record = mysql_fetch_assoc($result);
$userId = $record['userId'];
$avatar = $record['avatar'];

if(!$urlUsername){
	header("Location: login.php?redirection=restrictedContent");
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<!--head contains the .css file link, the favicon.ico link and the title-->
	<link rel="shortcut icon" href="images/favicon<? echo colourOfTheDay(); ?>.ico"/>
	<link rel="stylesheet" href="siteStyle.css"/>
	<script language="javascript" src="javascript.js"></script>
	<title><? echo $urlUsername; ?>'s Profile</title>
</head>

<body>
<div id="wrapper">
	<!--HEADER-->
	<div id="logo" onClick="location.href='index.php'">
		<img src="images/logo<? echo colourOfTheDay(); ?>.png" alt="logo image" title="Logo" width="50px" /> <h1>photomonkey</h1>
	</div>

	<div id="search">
		<form action="search.php?area=tags" method="post" >
				<input type="text" name="searchBox" class="input" value="Search..." onClick="this.value=''"/>
				<input type="submit" value="Go" class="button" />
		</form>
	</div>
	
	<div id="topNav">
		<ul id="menu">
			<li><a href="index.php" id="tabLink">home</a></li>
			<li>
				<a href="explore.php?content=potd" id="tabLink" onmouseover="menuOpen('dropDownMenu')" onmouseout="menuClose()">let's go exploring...</a>
				<div id="dropDownMenu" onmouseover="cancelClose('dropDownMenu')" onmouseout="menuClose()">
					<a href="explore.php?content=bbint">by interestingness</a>
					<a href="explore.php?content=bbrec">by most resent</a>
			        <a href="explore.php?content=bbcat">by category</a>
			        <a href="explore.php?content=bbspec">by spectrum</a>
			        <a href="explore.php?content=bbcotd">by colour of the day</a>
					<a href="explore.php?content=rand">randomly</a>
	        	</div>
			</li>
			<li>
				<a href="profile.php?username=<? echo $sessUsername; ?>" id="currentTabLink" onMouseOver="menuOpen('dropDownMenu2')" onMouseOut="menuClose()" >your profile</a>
				<div id="dropDownMenu2" onMouseOver="cancelClose('dropDownMenu2')" onMouseOut="menuClose()">
					<a href="userPhotos.php?username=<? echo $sessUsername;?>">your photos</a>
			        <a href="editAccount.php?userId=<? echo $sessUserId; ?>">adjust account</a>
	        	</div>
			</li>
			<li><a href="upload.php" id="tabLink">upload</a></li>
		</ul>

		<ul id="menu" class="floatRight">
			<?
			if($loggedIn){
				
				echo '<li><a href="logout.php" id="tabLink">logout '.$sessUsername.'</a></li>';
				if($sessUserType == 'a') {
					echo '<li><a href="admin.php" id="tabLink">admin</a></li>';
				}
			}
			else{
				echo '<li><a href="login.php" id="tabLink">login / sign up</a></li>';
			}
			?>
		</ul>
	</div>

	<!MAIN>
	<div id="main">
		<?
		//counts the number of results for the paging.
		$countQuery =	"SELECT COUNT(*)
						FROM photo
						WHERE ownerId = $userId";
		$countResult = mysql_query($countQuery);
		$countRecord = mysql_fetch_assoc($countResult);
		$photoCount = $countRecord['COUNT(*)'];

		$photosPerPage = 35;
		$noPages = ceil($photoCount/$photosPerPage);//the ceil function rounds up.

		$query =	"SELECT photoId, fileName, title
					FROM photo
					WHERE ownerId = $userId
					ORDER BY dateAdded DESC
					LIMIT ".(($page*$photosPerPage)-($photosPerPage)).",".($page*$photosPerPage);
		$result = mysql_query($query);

		echo "<img src=\"photos/$userId/$avatar\" alt=\"$urlUsername's avatar\" width=\"50\"/> ";
		if($sessUsername == $urlUsername && $sessUserId == $userId){
			echo "<h2>Your Photos</h2><br />";
		}
		else echo "<h2>$urlUsername's Photos</h2><br />";
		echo "<div id=\"full\">";
		echo "<table>";

		for ($rows = 0; $rows<5; $rows++){
			echo "<tr>";
			for($columns = 0; $columns<7; $columns++){

				$record = mysql_fetch_assoc($result);
				$fileName = $record['fileName'];
				$title = $record['title'];
				$photoId = $record['photoId'];
	
				if($record){
					echo "<td><a href=\"photo.php?photoId=$photoId\"><img src=\"photos/$userId/thumb_$fileName\" alt=\"$urlUsername's photo $fileName\" title=\"$title\" width=110 border=0 /></a>";
					if($sessUsername == $urlUsername && $sessUserId == $userId){
						echo "<br /><a href=\"editPhoto.php?fileName=$fileName\">Edit</a> | <a href=\"removePhoto.php?photoId=$photoId\">Remove</a></td>";
					}
				}
				else break;
			}
			echo "</tr>";
		}

		echo "</table><br />";
		
		if ($photoCount > 0) {
			//if there are photos present then the paging links are printed.
			if ($page > 1) echo "<a href=\"userPhotos.php?username=$urlUsername&page=".($page-1)."\">< Newer photos</a> | ";

			for ($i=1; $i<=$noPages; $i++){
				if($i == $page && $i == $noPages) echo "Page $i";
				else if($i == $page) echo "Page $i | ";
				else echo "<a href=\"userPhotos.php?username=$urlUsername&page=$i\">Page $i</a> | ";
			}

			if ($page < $noPages) echo "<a href=\"userPhotos.php?username=$urlUsername&page=".($page+1)."\">Older Photos ></a>";

		}
		else {
			//if there are no photos found then this is printed.
			echo "<i>There are no pictures uploaded yet.</i>";
		}

		echo "</div>";
		?>
		<div id="clear"></div>
	</div>

	<!--FOOTER-->
	<ul id="menu">
		<li><a href="about.php" id="tabLink">about</a></li>
	</ul>
</div>
</body>

</html>